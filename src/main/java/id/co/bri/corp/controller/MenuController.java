package id.co.bri.corp.controller;

import id.co.bri.corp.model.common.form.BaseFilter;
import id.co.bri.corp.model.core.FundTransfer;
import id.co.bri.corp.model.och.Disbursement;
import id.co.bri.corp.model.och.Registration;
import id.co.bri.corp.service.DisbursementService;
import id.co.bri.corp.service.FundTransferService;
import id.co.bri.corp.service.RegistrationService;
import id.co.bri.corp.util.JSPHeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;


@Controller
public class MenuController {
    @Autowired
    RegistrationService registrationService;
    @Autowired
    DisbursementService disbursementService;
    @Autowired
    FundTransferService fundTransferService;

    @RequestMapping(value = "form/group/registration")
    public ModelAndView registrationList(Authentication authentication, Map<String, Object> map) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            Optional<List<Registration>> registrations = Optional.of(registrationService.getAllRegistrations());
            modelAndView = Optional.of(pageRegistrationBuilder(registrations.get(), authentication));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/application")
    public ModelAndView application(Authentication authentication, Map<String, Object> map) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllApplications());
            modelAndView = Optional.of(pageApplicationBuilder(disbursements.get(), authentication));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/loanCreated")
    public ModelAndView loanCreated(Authentication authentication, Map<String, Object> map) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
            modelAndView = Optional.of(pageDisbursementBuilder(disbursements.get(), authentication));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/paidOff")
    public ModelAndView paidOff(Authentication authentication, Map<String, Object> map) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAllDisbursement());
            Optional<List<FundTransfer>> newFundTransfer = Optional.of(new ArrayList<>());
            Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanPaid());
            fundTransfers.get().stream().forEach(ft -> {
                disbursements.get().stream().forEach(db -> {
                    if (db.getLoan_account_id().equalsIgnoreCase(ft.getLoan_account())) {
                        ft.setDisbursement(db);
                        newFundTransfer.get().add(ft);
                    }
                });
            });
            modelAndView = Optional.of(pagePaidOffBuilder(newFundTransfer.get(), authentication));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/loanDisbursed")
    public ModelAndView loanDisbursed(Authentication authentication, Map<String, Object> map) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAllDisbursement());
            Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
            fundTransfers.get().stream().forEach(ft -> {
                Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                Disbursement disbursement = matchingObject.orElse(new Disbursement());
                ft.setDisbursement(disbursement);
            });
            modelAndView = Optional.of(pageFundTransferBuilder(fundTransfers.get(), authentication));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/loanDisbursedFiltered")
    public ModelAndView loanDisbursedFiltered(Authentication authentication, @ModelAttribute BaseFilter baseFilter) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            if (baseFilter.getLoanAccount().isEmpty() && baseFilter.getUserId().isEmpty() && baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getDisbursementFiltered(baseFilter, 2));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                    Disbursement disbursement = matchingObject.orElse(new Disbursement());
                    ft.setDisbursement(disbursement);
                });
                modelAndView = Optional.of(pageFundTransferBuilder(fundTransfers.get(), authentication));
            }
            else if (!baseFilter.getLoanAccount().isEmpty() && baseFilter.getUserId().isEmpty() && baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getDisbursementFiltered(baseFilter, 1));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                    Disbursement disbursement = matchingObject.orElse(new Disbursement());
                    ft.setDisbursement(disbursement);
                });
                modelAndView = Optional.of(pageFundTransferBuilder(fundTransfers.get(), authentication));
            }
            else if (baseFilter.getLoanAccount().isEmpty() && !baseFilter.getUserId().isEmpty() && baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> ftList = Optional.ofNullable(new ArrayList<>());
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getDisbursementFiltered(baseFilter, 2));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                    Disbursement disbursement = matchingObject.orElse(new Disbursement());
                    if (Objects.nonNull(disbursement)) {
                        if (disbursement.getUser_id().equals(baseFilter.getUserId())) {
                            ft.setDisbursement(disbursement);
                            ftList.get().add(ft);
                        }
                    }
                });
                modelAndView = Optional.of(pageRepaymentBuilder(ftList.get(), authentication));
            }
            else if (baseFilter.getLoanAccount().isEmpty() && baseFilter.getUserId().isEmpty() && !baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> ftList = Optional.ofNullable(new ArrayList<>());
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getDisbursementFiltered(baseFilter, 2));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    disbursements.get().stream().forEach(db -> {
                        if (Objects.nonNull(db)) {
                            if (db.getLoan_account_id().compareTo(ft.getLoan_account()) == 0) {
                                if (db.getPayrollDetail().getName_wl().startsWith(baseFilter.getAccountName())) {
                                    ft.setDisbursement(db);
                                    ftList.get().add(ft);
                                }
                            }
                        }
                    });
                });
                modelAndView = Optional.of(pageFundTransferBuilder(ftList.get(), authentication));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/repayment")
    public ModelAndView agfSuccess(Authentication authentication, Map<String, Object> map) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAllAgfs());
            Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
            fundTransfers.get().stream().forEach(ft -> {
                Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                Disbursement disbursement = matchingObject.orElse(new Disbursement());
                ft.setDisbursement(disbursement);
            });
            modelAndView = Optional.of(pageRepaymentBuilder(fundTransfers.get(), authentication));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    @RequestMapping(value = "form/group/repaymentFiltered")
    public ModelAndView repaymentFiltered(Authentication authentication, @ModelAttribute BaseFilter baseFilter) {
        Optional<ModelAndView> modelAndView = Optional.empty();
        try {
            if (baseFilter.getLoanAccount().isEmpty() && baseFilter.getUserId().isEmpty() && baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAllAgfs());
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                    Disbursement disbursement = matchingObject.orElse(new Disbursement());
                    ft.setDisbursement(disbursement);
                });
                modelAndView = Optional.of(pageRepaymentBuilder(fundTransfers.get(), authentication));
            }
            else if (!baseFilter.getLoanAccount().isEmpty() && baseFilter.getUserId().isEmpty() && baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAgfFiltered(baseFilter, 1));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                    Disbursement disbursement = matchingObject.orElse(new Disbursement());
                    ft.setDisbursement(disbursement);
                });
                modelAndView = Optional.of(pageRepaymentBuilder(fundTransfers.get(), authentication));
            }
            else if (baseFilter.getLoanAccount().isEmpty() && !baseFilter.getUserId().isEmpty() && baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> ftList = Optional.ofNullable(new ArrayList<>());
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAgfFiltered(baseFilter, 2));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    Optional<Disbursement> matchingObject = disbursements.get().stream().filter(p -> p.getLoan_account_id().equals(ft.getLoan_account())).findFirst();
                    Disbursement disbursement = matchingObject.orElse(new Disbursement());
                    if (Objects.nonNull(disbursement)) {
                        if (disbursement.getUser_id().equals(baseFilter.getUserId())) {
                            ft.setDisbursement(disbursement);
                            ftList.get().add(ft);
                        }
                    }
                });
                modelAndView = Optional.of(pageRepaymentBuilder(ftList.get(), authentication));
            }
            else if (baseFilter.getLoanAccount().isEmpty() && baseFilter.getUserId().isEmpty() && !baseFilter.getAccountName().isEmpty()) {
                Optional<List<FundTransfer>> ftList = Optional.ofNullable(new ArrayList<>());
                Optional<List<FundTransfer>> fundTransfers = Optional.of(fundTransferService.getAgfFiltered(baseFilter, 2));
                Optional<List<Disbursement>> disbursements = Optional.of(disbursementService.getAllLoanCreated());
                fundTransfers.get().stream().forEach(ft -> {
                    disbursements.get().stream().forEach(db -> {
                        if (Objects.nonNull(db)) {
                            if (db.getLoan_account_id().compareTo(ft.getLoan_account()) == 0) {
                                if (db.getPayrollDetail().getName_wl().startsWith(baseFilter.getAccountName())) {
                                    ft.setDisbursement(db);
                                    ftList.get().add(ft);
                                }
                            }
                        }
                    });
                });
                modelAndView = Optional.of(pageRepaymentBuilder(ftList.get(), authentication));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView.get();
    }

    private ModelAndView pageRegistrationBuilder(List<Registration> registrations, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formRegistration");
        modelAndView.get().addObject("registrationList", registrations);
        modelAndView.get().addObject("totalColumn", "4");
        modelAndView.get().addObject("totalRow", registrations.size());
        modelAndView.get().addObject("title", JSPHeaderUtil.getFormRegistrationHeader());
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }

    private ModelAndView pageApplicationBuilder(List<Disbursement> disbursements, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formApplication");
        modelAndView.get().addObject("disbursementList", disbursements);
        modelAndView.get().addObject("totalColumn", "12");
        modelAndView.get().addObject("totalRow", disbursements.size());
        modelAndView.get().addObject("title", JSPHeaderUtil.getFormApplicationHeader());
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }


    private ModelAndView pageDisbursementBuilder(List<Disbursement> disbursements, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formDisbursement");
        modelAndView.get().addObject("disbursementList", disbursements);
        modelAndView.get().addObject("totalColumn", "13");
        modelAndView.get().addObject("totalRow", disbursements.size());
        modelAndView.get().addObject("title", JSPHeaderUtil.getFormDisbursementHeader());
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }


    private ModelAndView pageFundTransferBuilder(List<FundTransfer> fundTransfers, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formFundTransfer");
        modelAndView.get().addObject("fundTransferList", fundTransfers);
        modelAndView.get().addObject("totalColumn", "13");
        modelAndView.get().addObject("totalRow", fundTransfers.size());
        modelAndView.get().addObject("title", JSPHeaderUtil.getFormFundTransferHeader());
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }

    private ModelAndView pagePaidOffBuilder(List<FundTransfer> fundTransfers, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formPaidOff");
        modelAndView.get().addObject("fundTransferList", fundTransfers);
        modelAndView.get().addObject("totalColumn", "10");
        modelAndView.get().addObject("totalRow", fundTransfers.size());
        modelAndView.get().addObject("title", JSPHeaderUtil.getFormPaidOffHeader());
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }

    private ModelAndView pageRepaymentBuilder(List<FundTransfer> fundTransfers, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formRepayment");
        modelAndView.get().addObject("fundTransferList", fundTransfers);
        modelAndView.get().addObject("totalColumn", "13");
        modelAndView.get().addObject("totalRow", fundTransfers.size());
        modelAndView.get().addObject("title", JSPHeaderUtil.getFormRepaymentHeader());
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }
}