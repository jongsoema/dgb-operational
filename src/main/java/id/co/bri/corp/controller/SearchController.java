package id.co.bri.corp.controller;

import id.co.bri.corp.model.common.form.BaseFilter;
import id.co.bri.corp.model.och.ApplicationDetail;
import id.co.bri.corp.service.ApplicationDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;


@Controller
public class SearchController {
    @Autowired
    ApplicationDetailService applicationDetailService;

    @RequestMapping(value = "form/group/cif")
    public ModelAndView searchNasabah(final Authentication authentication) {
        Optional<ApplicationDetail> applicationDetail = Optional.of(new ApplicationDetail());
        try {
            Optional<ModelAndView> modelAndView = Optional.of(pageCifBuilder(applicationDetail.get(), authentication));
            return modelAndView.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "form/group/cif/searched")
    public ModelAndView searchedNasabah(Authentication authentication, Map<String, Object> map, @RequestParam("userId") String userId) {
        Optional<ApplicationDetail> applicationDetail;
        try {
            if (Objects.nonNull(userId)) {
                applicationDetail = Optional.of(applicationDetailService.findById(userId));
            }
            else {
                applicationDetail = Optional.of(new ApplicationDetail());
            }
            Optional<ModelAndView> modelAndView = Optional.of(pageCifBuilder(applicationDetail.get(), authentication));
            return modelAndView.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private ModelAndView pageCifBuilder(ApplicationDetail applicationDetail, final Authentication authentication) {
        Optional<ModelAndView> modelAndView = Optional.of(new ModelAndView());
        modelAndView.get().setViewName("form/lv2/formCifInquiry");
        modelAndView.get().addObject("applicationDetail", applicationDetail);
        modelAndView.get().addObject("totalColumn", "13");
        modelAndView.get().addObject("principal", authentication.getName());
        modelAndView.get().addObject("baseFilter", new BaseFilter());
        return modelAndView.get();
    }
}