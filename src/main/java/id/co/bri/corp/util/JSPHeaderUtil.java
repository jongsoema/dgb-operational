package id.co.bri.corp.util;

public class JSPHeaderUtil {
    private static String formRegistrationHeader;
    private static String formDisbursementHeader;
    private static String formFundTransferHeader;
    private static String formApplicationHeader;
    private static String formRepaymentHeader;
    private static String formPaidOffHeader;

    static {
        formRegistrationHeader = new String("test,User ID,Time Created,Time Modified");
        formApplicationHeader = new String("test,User Id,NIK,Customer Name,Application Id,Req Loan Amt,Loan Amt Approved,Req Tenor,Application Status,Time Created,Time Modified,Company");
        formDisbursementHeader = new String("test,User Id,NIK,Customer Name,Loan Account,Loan Amt Disbursed,Tenor,Source Acct,Destination Acct,Trx Date,Remark,Disb Status,Company");
        formFundTransferHeader = new String("test,User Id,NIK,Customer Name,Loan Account,Loan Amt Disbursed,Tenor,Source Acct,Destination Acct,Trx Date,Remark,Disb Status,Company");
        formRepaymentHeader = new String("test,User Id,NIK,Customer Name,Loan Account,Installment Amt,Tenor,Source Acct,Destination Acct,Trx Date,Remark,Disb Status,Company");
        formPaidOffHeader = new String("test,User Id,NIK,Customer Name,Loan Account,Loan Amt Disbursed,Loan Amt Paid Off,Time Created,Time Modified,Company");

    }

    public static String getFormRegistrationHeader() {
        return formRegistrationHeader;
    }
    public static String getFormDisbursementHeader() { return formDisbursementHeader; }
    public static String getFormFundTransferHeader() { return formFundTransferHeader; }
    public static String getFormApplicationHeader() { return formApplicationHeader; }
    public static String getFormRepaymentHeader() {return formRepaymentHeader;}
    public static String getFormPaidOffHeader() {return formPaidOffHeader;}

}
