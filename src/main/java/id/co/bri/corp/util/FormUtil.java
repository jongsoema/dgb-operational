package id.co.bri.corp.util;

import java.util.*;

public class FormUtil {
    public static SortedSet<String> getRepaymentStatus() {
        Map<String, String> references = new HashMap<>();
        ArrayList<String> childReference = new ArrayList<>();

        childReference.add("SUCCESS");
        childReference.add("FAILED");
        childReference.add("PENDING");

        for (String s : childReference) {
            references.put(s, s);
        }
        SortedSet<String> values = new TreeSet<String>(references.values());
        return values;
    }

    public static SortedSet<String> getDisbursementStatus() {
        Map<String, String> references = new HashMap<>();
        ArrayList<String> childReference = new ArrayList<>();

        childReference.add("SUCCESS");
        childReference.add("FAILED");
        childReference.add("PENDING");

        for (String s : childReference) {
            references.put(s, s);
        }
        SortedSet<String> values = new TreeSet<String>(references.values());
        return values;
    }

    public static String numberToText(final Double number) {
        String val = String.format("%.0f", number);
        return val;
    }
}
