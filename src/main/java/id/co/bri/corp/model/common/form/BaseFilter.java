package id.co.bri.corp.model.common.form;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
public class BaseFilter implements Serializable {
    @Temporal(TemporalType.DATE)
    protected Date fromDate;

    @Temporal(TemporalType.DATE)
    protected Date toDate;

    protected String userId;

    protected String applicationId;

    protected String loanAccount;

    protected String status;

    protected String accountName;

    protected String companyName;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getLoanAccount() {
        return loanAccount;
    }

    public void setLoanAccount(String loanAccount) {
        this.loanAccount = loanAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return "BaseFilter{" +
                "fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", userId='" + userId + '\'' +
                ", applicationId='" + applicationId + '\'' +
                ", loanAccount='" + loanAccount + '\'' +
                ", status='" + status + '\'' +
                ", accountName='" + accountName + '\'' +
                ", companyName='" + companyName + '\'' +
                '}';
    }
}
