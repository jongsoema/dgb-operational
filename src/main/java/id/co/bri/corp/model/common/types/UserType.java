package id.co.bri.corp.model.common.types;

public enum UserType {
    RETAILUSER(1),
    ADMIN(4),
    OSUSER(5),
    UNDEFINE(99);

    private Integer userType;

    UserType(final Integer userType){
        this.userType = userType;
    }

    public Integer getUserType() {
        return userType;
    }

    @Override
    public String toString(){
        return String.valueOf(this.userType);
    }

    public String getName(){
        return this.name();
    }
}
