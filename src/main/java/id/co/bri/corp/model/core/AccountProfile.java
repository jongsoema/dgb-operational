package id.co.bri.corp.model.core;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "crmuser.accounts")
public class AccountProfile implements Serializable {
    @Id
    @Column(name = "accountid")
    protected String accountid;

    @Column(name = "maidennameofmother")
    protected String maidennameofmother;

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public String getMaidennameofmother() {
        return maidennameofmother;
    }

    public void setMaidennameofmother(String maidennameofmother) {
        this.maidennameofmother = maidennameofmother;
    }

    @Override
    public String toString() {
        return "AccountProfile{" +
                "accountid='" + accountid + '\'' +
                ", maidennameofmother='" + maidennameofmother + '\'' +
                '}';
    }
}
