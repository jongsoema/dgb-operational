package id.co.bri.corp.model.och;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "state_codes")
public class StateCodes implements Serializable {
    @Column(name = "cntry")
    protected String cntry;

    @Id
    @Column(name = "state_code")
    protected String state_code;

    @Column(name = "state_desc")
    protected String state_desc;


    public String getCntry() {
        return cntry;
    }

    public void setCntry(String cntry) {
        this.cntry = cntry;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getState_desc() {
        return state_desc;
    }

    public void setState_desc(String state_desc) {
        this.state_desc = state_desc;
    }

    @Override
    public String toString() {
        return "StateCodes{" +
                "cntry='" + cntry + '\'' +
                ", state_code='" + state_code + '\'' +
                ", state_desc='" + state_desc + '\'' +
                '}';
    }
}
