package id.co.bri.corp.model.och.composite;


import java.io.Serializable;

public class EformMapPK implements Serializable {
    protected String key;
    protected String name;

    public EformMapPK() {
    }

    public EformMapPK(String key, String name) {
        this.key = key;
        this.name = name;
    }
}
