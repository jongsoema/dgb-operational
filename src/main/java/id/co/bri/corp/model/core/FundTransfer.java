package id.co.bri.corp.model.core;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import id.co.bri.corp.model.och.Disbursement;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "custom.fundtrf_sts_tbl")
public class FundTransfer implements Serializable {
    @Id
    @Column(name = "msg_ref_num")
    protected String msg_ref_num;

    @JsonIgnore
    @Transient
    protected Disbursement disbursement;

    @Column(name = "loan_account")
    protected String loan_account;

    @Column(name = "source_of_fund")
    protected String source_of_fund;

    @Column(name = "destination_of_fund")
    protected String destination_of_fund;

    @Column(name = "transfer_type")
    protected String transfer_type;

    @JsonIgnore
    @Column(name = "transaction_date_time")
    protected Timestamp transaction_date_time;

    @Column(name = "amount")
    protected String amount;

    @Column(name = "transaction_remarks")
    protected String transaction_remarks;

    @Column(name = "customer_name")
    protected String customer_name;

    @Column(name = "status_code")
    protected String status_code;

    @Column(name = "status")
    protected String status;

    @Column(name = "bank_id")
    protected String bank_id;

    @Column(name = "free_text1")
    protected String free_text1;

    @Column(name = "free_text2")
    protected String free_text2;

    @Column(name = "free_text3")
    protected String free_text3;

    @Column(name = "free_text4")
    protected String free_text4;

    @Column(name = "del_flg")
    protected String del_flg;

    @Column(name = "entity_cre_flg")
    protected String entity_cre_flg;

    @Column(name = "lchg_user_id")
    protected String lchg_user_id;

    @JsonIgnore
    @Column(name = "lchg_time")
    protected Timestamp lchg_time;

    @Column(name = "rcre_user_id")
    protected String rcre_user_id;

    @JsonIgnore
    @Column(name = "rcre_time")
    protected Timestamp rcre_time;

    public String getMsg_ref_num() {
        return msg_ref_num;
    }

    public void setMsg_ref_num(String msg_ref_num) {
        this.msg_ref_num = msg_ref_num;
    }

    public String getLoan_account() {
        return loan_account;
    }

    public void setLoan_account(String loan_account) {
        this.loan_account = loan_account;
    }

    public String getSource_of_fund() {
        return source_of_fund;
    }

    public void setSource_of_fund(String source_of_fund) {
        this.source_of_fund = source_of_fund;
    }

    public String getDestination_of_fund() {
        return destination_of_fund;
    }

    public void setDestination_of_fund(String destination_of_fund) {
        this.destination_of_fund = destination_of_fund;
    }

    public String getTransfer_type() {
        return transfer_type;
    }

    public void setTransfer_type(String transfer_type) {
        this.transfer_type = transfer_type;
    }

    public Timestamp getTransaction_date_time() {
        return transaction_date_time;
    }

    public void setTransaction_date_time(Timestamp transaction_date_time) {
        this.transaction_date_time = transaction_date_time;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransaction_remarks() {
        return transaction_remarks;
    }

    public void setTransaction_remarks(String transaction_remarks) {
        this.transaction_remarks = transaction_remarks;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBank_id() {
        return bank_id;
    }

    public void setBank_id(String bank_id) {
        this.bank_id = bank_id;
    }

    public String getFree_text1() {
        return free_text1;
    }

    public void setFree_text1(String free_text1) {
        this.free_text1 = free_text1;
    }

    public String getFree_text2() {
        return free_text2;
    }

    public void setFree_text2(String free_text2) {
        this.free_text2 = free_text2;
    }

    public String getFree_text3() {
        return free_text3;
    }

    public void setFree_text3(String free_text3) {
        this.free_text3 = free_text3;
    }

    public String getFree_text4() {
        return free_text4;
    }

    public void setFree_text4(String free_text4) {
        this.free_text4 = free_text4;
    }

    public String getDel_flg() {
        return del_flg;
    }

    public void setDel_flg(String del_flg) {
        this.del_flg = del_flg;
    }

    public String getEntity_cre_flg() {
        return entity_cre_flg;
    }

    public void setEntity_cre_flg(String entity_cre_flg) {
        this.entity_cre_flg = entity_cre_flg;
    }

    public String getLchg_user_id() {
        return lchg_user_id;
    }

    public void setLchg_user_id(String lchg_user_id) {
        this.lchg_user_id = lchg_user_id;
    }

    public Timestamp getLchg_time() {
        return lchg_time;
    }

    public void setLchg_time(Timestamp lchg_time) {
        this.lchg_time = lchg_time;
    }

    public String getRcre_user_id() {
        return rcre_user_id;
    }

    public void setRcre_user_id(String rcre_user_id) {
        this.rcre_user_id = rcre_user_id;
    }

    public Timestamp getRcre_time() {
        return rcre_time;
    }

    public void setRcre_time(Timestamp rcre_time) {
        this.rcre_time = rcre_time;
    }

    public Disbursement getDisbursement() {
        return disbursement;
    }

    public void setDisbursement(Disbursement disbursement) {
        this.disbursement = disbursement;
    }


    @Override
    public String toString() {
        return "FundTransfer{" +
                "msg_ref_num='" + msg_ref_num + '\'' +
                ", disbursement=" + disbursement +
                ", loan_account='" + loan_account + '\'' +
                ", source_of_fund='" + source_of_fund + '\'' +
                ", destination_of_fund='" + destination_of_fund + '\'' +
                ", transfer_type='" + transfer_type + '\'' +
                ", transaction_date_time=" + transaction_date_time +
                ", amount='" + amount + '\'' +
                ", transaction_remarks='" + transaction_remarks + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", status_code='" + status_code + '\'' +
                ", status='" + status + '\'' +
                ", bank_id='" + bank_id + '\'' +
                ", free_text1='" + free_text1 + '\'' +
                ", free_text2='" + free_text2 + '\'' +
                ", free_text3='" + free_text3 + '\'' +
                ", free_text4='" + free_text4 + '\'' +
                ", del_flg='" + del_flg + '\'' +
                ", entity_cre_flg='" + entity_cre_flg + '\'' +
                ", lchg_user_id='" + lchg_user_id + '\'' +
                ", lchg_time=" + lchg_time +
                ", rcre_user_id='" + rcre_user_id + '\'' +
                ", rcre_time=" + rcre_time +
                '}';
    }
}
