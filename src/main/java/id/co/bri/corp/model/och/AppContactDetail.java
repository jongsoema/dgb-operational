package id.co.bri.corp.model.och;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "custom_loan_app_contact_detals")
public class AppContactDetail implements Serializable {
    @Id
    @Column(name = "application_id")
    protected Integer application_id;

    @Column(name = "mother_name")
    protected String mother_name;

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    @Override
    public String toString() {
        return "AppContactDetail{" +
                "application_id=" + application_id +
                ", mother_name='" + mother_name + '\'' +
                '}';
    }
}
