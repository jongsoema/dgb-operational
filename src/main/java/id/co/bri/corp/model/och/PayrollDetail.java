package id.co.bri.corp.model.och;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import id.co.bri.corp.model.och.ApplicationDetail;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "custom_loan_appln_payroll_acct")
public class PayrollDetail implements Serializable {
    @Id
    @Column(name = "application_id")
    protected Integer application_id;

    @Column(name = "payroll_account_num")
    protected String payroll_account_num;

    @Column(name = "payroll_acc_branch")
    protected String payroll_acc_branch;

    @JsonIgnore
    @Column(name = "acc_payroll_date")
    protected Timestamp acc_payroll_date;

    @Column(name = "refferal_code")
    protected String refferal_code;

    @Column(name = "agent_code")
    protected String agent_code;

    @Column(name = "company_name")
    protected String company_name;

    @Column(name = "name_wl")
    protected String name_wl;

    @Column(name = "personel_number")
    protected String personel_number;

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public String getPayroll_account_num() {
        return payroll_account_num;
    }

    public void setPayroll_account_num(String payroll_account_num) {
        this.payroll_account_num = payroll_account_num;
    }

    public String getPayroll_acc_branch() {
        return payroll_acc_branch;
    }

    public void setPayroll_acc_branch(String payroll_acc_branch) {
        this.payroll_acc_branch = payroll_acc_branch;
    }

    public Timestamp getAcc_payroll_date() {
        return acc_payroll_date;
    }

    public void setAcc_payroll_date(Timestamp acc_payroll_date) {
        this.acc_payroll_date = acc_payroll_date;
    }

    public String getRefferal_code() {
        return refferal_code;
    }

    public void setRefferal_code(String refferal_code) {
        this.refferal_code = refferal_code;
    }

    public String getAgent_code() {
        return agent_code;
    }

    public void setAgent_code(String agent_code) {
        this.agent_code = agent_code;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getName_wl() {
        return name_wl;
    }

    public void setName_wl(String name_wl) {
        this.name_wl = name_wl;
    }

    public String getPersonel_number() {
        return personel_number;
    }

    public void setPersonel_number(String personel_number) {
        this.personel_number = personel_number;
    }
}
