package id.co.bri.corp.model.och;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "custom_loan_app_applicant_dlts")
public class ApplicationDetail implements Serializable {
    @Column(name = "db_ts")
    protected Integer dbTs;

    @Column(name = "bank_id")
    protected String bank_id;

    @Id
    @Column(name = "application_id")
    protected Integer application_id;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "application_id",insertable=false, updatable=false)
    protected AppContactDetail appContactDetail;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "state",insertable=false, updatable=false)
    protected StateCodes province;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "city",insertable=false, updatable=false)
    protected CommonCodes customerCity;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "addr_line_4",insertable=false, updatable=false)
    protected CommonCodes kelurahan;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "addr_line_5",insertable=false, updatable=false)
    protected CommonCodes kecamatan;

    @Column(name = "email_id")
    protected String email_id;

    @Column(name = "mobile_no")
    protected Long mobile_no;

    @Column(name = "name")
    protected String name;

    @Column(name = "national_id")
    protected String national_id;

    @Column(name = "gender")
    protected String gender;

    @Column(name = "place_of_birth")
    protected String place_of_birth;

    @Column(name = "religion")
    protected String religion;

    @Column(name = "last_education")
    protected String last_education;

    @Column(name = "home_ownership_status")
    protected String home_ownership_status;

    @Column(name = "home_ownership_duration")
    protected Integer home_ownership_duration;

    @Column(name = "home_phone_num")
    protected Long home_phone_num;

    @Column(name = "addr_line_1")
    protected String addr_line_1;

    @Column(name = "addr_line_2")
    protected String addr_line_2;

    @Column(name = "addr_line_3")
    protected String addr_line_3;

    @Column(name = "addr_line_4")
    protected String addr_line_4;

    @Column(name = "addr_line_5")
    protected String addr_line_5;

    @Column(name = "state")
    protected String state;

    @Column(name = "city")
    protected String city;

    @Column(name = "postal_code")
    protected Integer postal_code;

    @Column(name = "card_issuer_bank")
    protected String card_issuer_bank;

    @JsonIgnore
    @Column(name = "date_of_birth")
    protected Timestamp date_of_birth;

    @Column(name = "free_txt1")
    protected String free_txt1;

    @Column(name = "free_txt2")
    protected String free_txt2;

    @Column(name = "free_txt3")
    protected String free_txt3;

    @JsonIgnore
    @Column(name = "free_date1")
    protected Timestamp free_date1;

    @JsonIgnore
    @Column(name = "free_date2")
    protected Timestamp free_date2;

    @Column(name = "del_flg")
    protected String del_flg;

    @Column(name = "r_mod_id")
    protected String r_mod_id;

    @JsonIgnore
    @Column(name = "r_mod_time")
    protected Timestamp r_mod_time;

    @Column(name = "r_cre_id")
    protected String r_cre_id;

    @JsonIgnore
    @Column(name = "r_cre_time")
    protected Timestamp r_cre_time;

    public Integer getDbTs() {
        return dbTs;
    }

    public void setDbTs(Integer dbTs) {
        this.dbTs = dbTs;
    }

    public String getBank_id() {
        return bank_id;
    }

    public void setBank_id(String bank_id) {
        this.bank_id = bank_id;
    }

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public Long getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(Long mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        this.national_id = national_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getLast_education() {
        return last_education;
    }

    public void setLast_education(String last_education) {
        this.last_education = last_education;
    }

    public String getHome_ownership_status() {
        return home_ownership_status;
    }

    public void setHome_ownership_status(String home_ownership_status) {
        this.home_ownership_status = home_ownership_status;
    }

    public Integer getHome_ownership_duration() {
        return home_ownership_duration;
    }

    public void setHome_ownership_duration(Integer home_ownership_duration) {
        this.home_ownership_duration = home_ownership_duration;
    }

    public Long getHome_phone_num() {
        return home_phone_num;
    }

    public void setHome_phone_num(Long home_phone_num) {
        this.home_phone_num = home_phone_num;
    }

    public String getAddr_line_1() {
        return addr_line_1;
    }

    public void setAddr_line_1(String addr_line_1) {
        this.addr_line_1 = addr_line_1;
    }

    public String getAddr_line_2() {
        return addr_line_2;
    }

    public void setAddr_line_2(String addr_line_2) {
        this.addr_line_2 = addr_line_2;
    }

    public String getAddr_line_3() {
        return addr_line_3;
    }

    public void setAddr_line_3(String addr_line_3) {
        this.addr_line_3 = addr_line_3;
    }

    public String getAddr_line_4() {
        return addr_line_4;
    }

    public void setAddr_line_4(String addr_line_4) {
        this.addr_line_4 = addr_line_4;
    }

    public String getAddr_line_5() {
        return addr_line_5;
    }

    public void setAddr_line_5(String addr_line_5) {
        this.addr_line_5 = addr_line_5;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(Integer postal_code) {
        this.postal_code = postal_code;
    }

    public String getCard_issuer_bank() {
        return card_issuer_bank;
    }

    public void setCard_issuer_bank(String card_issuer_bank) {
        this.card_issuer_bank = card_issuer_bank;
    }

    public Timestamp getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Timestamp date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getFree_txt1() {
        return free_txt1;
    }

    public void setFree_txt1(String free_txt1) {
        this.free_txt1 = free_txt1;
    }

    public String getFree_txt2() {
        return free_txt2;
    }

    public void setFree_txt2(String free_txt2) {
        this.free_txt2 = free_txt2;
    }

    public String getFree_txt3() {
        return free_txt3;
    }

    public void setFree_txt3(String free_txt3) {
        this.free_txt3 = free_txt3;
    }

    public Timestamp getFree_date1() {
        return free_date1;
    }

    public void setFree_date1(Timestamp free_date1) {
        this.free_date1 = free_date1;
    }

    public Timestamp getFree_date2() {
        return free_date2;
    }

    public void setFree_date2(Timestamp free_date2) {
        this.free_date2 = free_date2;
    }

    public String getDel_flg() {
        return del_flg;
    }

    public void setDel_flg(String del_flg) {
        this.del_flg = del_flg;
    }

    public String getR_mod_id() {
        return r_mod_id;
    }

    public void setR_mod_id(String r_mod_id) {
        this.r_mod_id = r_mod_id;
    }

    public Timestamp getR_mod_time() {
        return r_mod_time;
    }

    public void setR_mod_time(Timestamp r_mod_time) {
        this.r_mod_time = r_mod_time;
    }

    public String getR_cre_id() {
        return r_cre_id;
    }

    public void setR_cre_id(String r_cre_id) {
        this.r_cre_id = r_cre_id;
    }

    public Timestamp getR_cre_time() {
        return r_cre_time;
    }

    public void setR_cre_time(Timestamp r_cre_time) {
        this.r_cre_time = r_cre_time;
    }

    public AppContactDetail getAppContactDetail() {
        return appContactDetail;
    }

    public void setAppContactDetail(AppContactDetail appContactDetail) {
        this.appContactDetail = appContactDetail;
    }

    public StateCodes getProvince() {
        return province;
    }

    public void setProvince(StateCodes province) {
        this.province = province;
    }

    public CommonCodes getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(CommonCodes customerCity) {
        this.customerCity = customerCity;
    }

    public CommonCodes getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(CommonCodes kelurahan) {
        this.kelurahan = kelurahan;
    }

    public CommonCodes getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(CommonCodes kecamatan) {
        this.kecamatan = kecamatan;
    }

    @Override
    public String toString() {
        return "ApplicationDetail{" +
                "dbTs=" + dbTs +
                ", bank_id='" + bank_id + '\'' +
                ", application_id=" + application_id +
                ", appContactDetail=" + appContactDetail +
                ", province=" + province +
                ", customerCity=" + customerCity +
                ", kelurahan=" + kelurahan +
                ", kecamatan=" + kecamatan +
                ", email_id='" + email_id + '\'' +
                ", mobile_no=" + mobile_no +
                ", name='" + name + '\'' +
                ", national_id='" + national_id + '\'' +
                ", gender='" + gender + '\'' +
                ", place_of_birth='" + place_of_birth + '\'' +
                ", religion='" + religion + '\'' +
                ", last_education='" + last_education + '\'' +
                ", home_ownership_status='" + home_ownership_status + '\'' +
                ", home_ownership_duration=" + home_ownership_duration +
                ", home_phone_num=" + home_phone_num +
                ", addr_line_1='" + addr_line_1 + '\'' +
                ", addr_line_2='" + addr_line_2 + '\'' +
                ", addr_line_3='" + addr_line_3 + '\'' +
                ", addr_line_4='" + addr_line_4 + '\'' +
                ", addr_line_5='" + addr_line_5 + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", postal_code=" + postal_code +
                ", card_issuer_bank='" + card_issuer_bank + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", free_txt1='" + free_txt1 + '\'' +
                ", free_txt2='" + free_txt2 + '\'' +
                ", free_txt3='" + free_txt3 + '\'' +
                ", free_date1=" + free_date1 +
                ", free_date2=" + free_date2 +
                ", del_flg='" + del_flg + '\'' +
                ", r_mod_id='" + r_mod_id + '\'' +
                ", r_mod_time=" + r_mod_time +
                ", r_cre_id='" + r_cre_id + '\'' +
                ", r_cre_time=" + r_cre_time +
                '}';
    }
}
