package id.co.bri.corp.model.och;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "corporate_user")
public class Registration implements Serializable {
    @Column(name = "db_ts")
    protected Integer dbTs;

    @Id
    @Column(name = "bank_id")
    protected String bankId;

    @Column(name = "org_id")
    protected String orgId;

    @Id
    @Column(name = "user_id")
    protected String userId;

    @JsonIgnore
    @Column(name = "user_type")
    protected String userType;

    @JsonIgnore
    @Column(name = "alert_user_category")
    protected String alertUserCategory;

    @JsonIgnore
    @Column(name = "cust_id")
    protected String customerId;

    @Column(name = "salutation")
    protected String salutation;

    @Column(name = "c_l_name")
    protected String lastName;

    @JsonIgnore
    @Column(name = "c_m_name")
    protected String middleName;

    @JsonIgnore
    @Column(name = "c_f_name")
    protected String firstName;

    @JsonIgnore
    @Column(name = "lang_c_l_name")
    protected String langCLName;

    @JsonIgnore
    @Column(name = "lang_c_m_name")
    protected String langCMName;

    @JsonIgnore
    @Column(name = "lang_c_f_name")
    protected String langCFName;

    @Column(name = "c_email_id")
    protected String emailId;

    @Column(name = "c_addr1")
    protected String addres1;

    @JsonIgnore
    @Column(name = "c_addr2")
    protected String addres2;

    @JsonIgnore
    @Column(name = "c_addr3")
    protected String addres3;

    @Column(name = "lang_c_addr1")
    protected String langCaddr1;

    @JsonIgnore
    @Column(name = "lang_c_addr2")
    protected String langCaddr2;

    @JsonIgnore
    @Column(name = "lang_c_addr3")
    protected String langCaddr3;

    @JsonIgnore
    @Column(name = "lang_c_city")
    protected String langCcity;

    @JsonIgnore
    @Column(name = "c_state")
    protected String c_state;

    @Column(name = "c_cntry")
    protected String country;

    @Column(name = "c_zip")
    protected String zipCode;

    @JsonIgnore
    @Column(name = "c_phone_no")
    protected String c_phone_no;

    @Column(name = "c_m_phone_no")
    protected String mobilePhoneNumber;

    @JsonIgnore
    @Column(name = "ddt_from")
    protected Integer ddt_from;

    @JsonIgnore
    @Column(name = "ddt_to")
    protected Integer ddt_to;

    @JsonIgnore
    @Column(name = "c_fax_no")
    protected String c_fax_no;

    @JsonIgnore
    @Column(name = "lang_id")
    protected String lang_id;

    @JsonIgnore
    @Column(name = "adm_id")
    protected String adm_id;

    @JsonIgnore
    @Column(name = "dt_fmt")
    protected String dt_fmt;

    @JsonIgnore
    @Column(name = "amt_fmt_cd")
    protected String amt_fmt_cd;

    @JsonIgnore
    @Column(name = "usr_int_cd")
    protected String usr_int_cd;

    @JsonIgnore
    @Column(name = "inc_rng_cd")
    protected String inc_rng_cd;

    @JsonIgnore
    @Column(name = "prim_acid")
    protected String prim_acid;

    @JsonIgnore
    @Column(name = "p_branch_id")
    protected String p_branch_id;

    @JsonIgnore
    @Column(name = "p_div_id")
    protected String p_div_id;

    @JsonIgnore
    @Column(name = "loc_acc_ind")
    protected String loc_acc_ind;

    @JsonIgnore
    @Column(name = "div_acc_ind")
    protected String div_acc_ind;

    @JsonIgnore
    @JsonProperty("last_txn_date")
    protected Timestamp last_txn_date;

    @JsonIgnore
    @Column(name = "sms_pwd")
    protected String sms_pwd;

    @JsonIgnore
    @Column(name = "sms_no_of_atmpts")
    protected Integer sms_no_of_atmpts;

    @JsonIgnore
    @Column(name = "login_date")
    protected Timestamp login_date;

    @JsonIgnore
    @Column(name = "logoff_date")
    protected Timestamp logoff_date;

    @JsonIgnore
    @Column(name = "session_id")
    protected String session_id;

    @JsonIgnore
    @Column(name = "login_cert_match_reqd")
    protected String login_cert_match_reqd;

    @JsonIgnore
    @Column(name = "txn_cert_match_reqd")
    protected String txn_cert_match_reqd;

    @JsonIgnore
    @Column(name = "multi_crn_txn_allowed")
    protected String multi_crn_txn_allowed;

    @JsonIgnore
    @Column(name = "ac_min_bal")
    protected Double ac_min_bal;

    @JsonIgnore
    @Column(name = "txn_limit")
    protected Double txn_limit;

    @JsonIgnore
    @Column(name = "txn_lmt_currency")
    protected String txn_lmt_currency;

    @JsonIgnore
    @Column(name = "sms_alert")
    protected String sms_alert;

    @JsonIgnore
    @Column(name = "ext_alert")
    protected String ext_alert;

    @JsonIgnore
    @Column(name = "default_approver")
    protected String default_approver;

    @JsonIgnore
    @Column(name = "correspondence_address")
    protected String correspondence_address;

    @JsonIgnore
    @Column(name = "tot_num_login")
    protected Integer tot_num_login;

    @JsonIgnore
    @Column(name = "avail_lang")
    protected String avail_lang;

    @JsonIgnore
    @Column(name = "ac_opn_dt")
    protected Timestamp ac_opn_dt;

    @JsonIgnore
    @Column(name = "c_gender")
    protected String c_gender;

    @JsonIgnore
    @Column(name = "date_of_birth")
    protected Timestamp date_of_birth;

    @JsonIgnore
    @Column(name = "marital_status")
    protected String marital_status;

    @JsonIgnore
    @Column(name = "anniversary_date")
    protected Timestamp anniversary_date;

    @JsonIgnore
    @Column(name = "num_household")
    protected String num_household;


    @JsonIgnore
    @Column(name = "occupation")
    protected String occupation;

    @JsonIgnore
    @Column(name = "category_code")
    protected String category_code;

    @JsonIgnore
    @Column(name = "limit_scheme")
    protected String limit_scheme;

    @JsonIgnore
    @Column(name = "educational_level")
    protected String educational_level;

    @JsonIgnore
    @Column(name = "passport_number")
    protected String passport_number;

    @JsonIgnore
    @Column(name = "passport_issue_date")
    protected Timestamp passport_issue_date;

    @JsonIgnore
    @Column(name = "passport_details")
    protected String passport_details;

    @JsonIgnore
    @Column(name = "passport_expiry_date")
    protected Timestamp passport_expiry_date;

    @JsonIgnore
    @Column(name = "pan_national_id")
    protected String pan_national_id;

    @JsonIgnore
    @Column(name = "cust_status")
    protected String cust_status;

    @JsonIgnore
    @Column(name = "c_res_status")
    protected String c_res_status;

    @JsonIgnore
    @Column(name = "online_reg_pwd")
    protected String online_reg_pwd;

    @JsonIgnore
    @Column(name = "free_text_1")
    protected String free_text_1;

    @JsonIgnore
    @Column(name = "free_text_2")
    protected String free_text_2;

    @JsonIgnore
    @Column(name = "free_text_3")
    protected String free_text_3;

    @JsonIgnore
    @Column(name = "free_text_4")
    protected String free_text_4;

    @JsonIgnore
    @Column(name = "oof_flg")
    protected String oof_flg;

    @JsonIgnore
    @Column(name = "del_flg")
    protected String del_flg;

    @JsonIgnore
    @Column(name = "r_mod_id")
    protected String r_mod_id;

    @JsonIgnore
    @Column(name = "r_mod_time")
    protected Timestamp r_mod_time;

    @JsonIgnore
    @Column(name = "r_cre_id")
    protected String r_cre_id;

    @JsonIgnore
    @Column(name = "r_cre_time")
    protected Timestamp r_cre_time;

    @JsonIgnore
    @Column(name = "page_scheme")
    protected String page_scheme;

    @JsonIgnore
    @Column(name = "authentication_mode")
    protected String authentication_mode;

    @JsonIgnore
    @Column(name = "authorization_mode")
    protected String authorization_mode;

    @JsonIgnore
    @Column(name = "user_id_type")
    protected String user_id_type;

    @JsonIgnore
    @Column(name = "ret_auth_mode_prec")
    protected String ret_auth_mode_prec;

    @JsonIgnore
    @Column(name = "login_allowed")
    protected String login_allowed;

    @JsonIgnore
    @Column(name = "transaction_allowed")
    protected String transaction_allowed;

    @JsonIgnore
    @Column(name = "auth_user")
    protected String auth_user;

    @JsonIgnore
    @Column(name = "range_limit_scheme")
    protected String range_limit_scheme;

    @JsonIgnore
    @Column(name = "wf_rule_select_auth")
    protected String wf_rule_select_auth;

    @JsonIgnore
    @Column(name = "last_unsuccessful_login_time")
    protected Timestamp last_unsuccessful_login_time;

    @JsonIgnore
    @Column(name = "user_encryption_key")
    protected String user_encryption_key;

    @JsonIgnore
    @Column(name = "force_terms_flag")
    protected String force_terms_flag;

    @JsonIgnore
    @Column(name = "user_txn_types")
    protected String user_txn_types;

    @JsonIgnore
    @Column(name = "user_alert_registration_flag")
    protected String user_alert_registration_flag;

    @JsonIgnore
    @Column(name = "scheme_id")
    protected String scheme_id;

    @JsonIgnore
    @Column(name = "alert_cust_id")
    protected String alert_cust_id;

    @JsonIgnore
    @Column(name = "concur_access_flg")
    protected String concur_access_flg;

    @JsonIgnore
    @Column(name = "virtual_user_flg")
    protected String virtual_user_flg;

    @JsonIgnore
    @Column(name = "tran_auth_scheme")
    protected String tran_auth_scheme;

    @JsonIgnore
    @Column(name = "cp_entity_counter")
    protected Integer cp_entity_counter;

    @JsonIgnore
    @Column(name = "individual_id")
    protected String individual_id;

    @JsonIgnore
    @Column(name = "acc_view_ind")
    protected String acc_view_ind;

    @JsonIgnore
    @Column(name = "nick_name")
    protected String nick_name;

    @JsonIgnore
    @Column(name = "cust_asst_menu_prf")
    protected String cust_asst_menu_prf;

    @JsonIgnore
    @Column(name = "cal_type")
    protected String cal_type;

    @JsonIgnore
    @Column(name = "acc_fmt")
    protected String acc_fmt;

    @JsonIgnore
    @Column(name = "segment_name")
    protected String segment_name;

    @JsonIgnore
    @Column(name = "access_scheme")
    protected String access_scheme;

    @JsonIgnore
    @Column(name = "account_masking_required_flag")
    protected String account_masking_required_flag;

    @JsonIgnore
    @Column(name = "login_channel")
    protected String login_channel;

    @JsonIgnore
    @Column(name = "logoff_channel")
    protected String logoff_channel;

    @JsonIgnore
    @Column(name = "last_unsuccessful_lgn_channel")
    protected String last_unsuccessful_lgn_channel;

    @JsonIgnore
    @Column(name = "confidential_txn_enabled")
    protected String confidential_txn_enabled;

    @JsonIgnore
    @Column(name = "prelogin_enabled_flag")
    protected String prelogin_enabled_flag;

    public Integer getDbTs() {
        return dbTs;
    }

    public void setDbTs(Integer dbTs) {
        this.dbTs = dbTs;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAlertUserCategory() {
        return alertUserCategory;
    }

    public void setAlertUserCategory(String alertUserCategory) {
        this.alertUserCategory = alertUserCategory;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLangCLName() {
        return langCLName;
    }

    public void setLangCLName(String langCLName) {
        this.langCLName = langCLName;
    }

    public String getLangCMName() {
        return langCMName;
    }

    public void setLangCMName(String langCMName) {
        this.langCMName = langCMName;
    }

    public String getLangCFName() {
        return langCFName;
    }

    public void setLangCFName(String langCFName) {
        this.langCFName = langCFName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAddres1() {
        return addres1;
    }

    public void setAddres1(String addres1) {
        this.addres1 = addres1;
    }

    public String getAddres2() {
        return addres2;
    }

    public void setAddres2(String addres2) {
        this.addres2 = addres2;
    }

    public String getAddres3() {
        return addres3;
    }

    public void setAddres3(String addres3) {
        this.addres3 = addres3;
    }

    public String getLangCaddr1() {
        return langCaddr1;
    }

    public void setLangCaddr1(String langCaddr1) {
        this.langCaddr1 = langCaddr1;
    }

    public String getLangCaddr2() {
        return langCaddr2;
    }

    public void setLangCaddr2(String langCaddr2) {
        this.langCaddr2 = langCaddr2;
    }

    public String getLangCaddr3() {
        return langCaddr3;
    }

    public void setLangCaddr3(String langCaddr3) {
        this.langCaddr3 = langCaddr3;
    }

    public String getLangCcity() {
        return langCcity;
    }

    public void setLangCcity(String langCcity) {
        this.langCcity = langCcity;
    }

    public String getC_state() {
        return c_state;
    }

    public void setC_state(String c_state) {
        this.c_state = c_state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getC_phone_no() {
        return c_phone_no;
    }

    public void setC_phone_no(String c_phone_no) {
        this.c_phone_no = c_phone_no;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public Integer getDdt_from() {
        return ddt_from;
    }

    public void setDdt_from(Integer ddt_from) {
        this.ddt_from = ddt_from;
    }

    public Integer getDdt_to() {
        return ddt_to;
    }

    public void setDdt_to(Integer ddt_to) {
        this.ddt_to = ddt_to;
    }

    public String getC_fax_no() {
        return c_fax_no;
    }

    public void setC_fax_no(String c_fax_no) {
        this.c_fax_no = c_fax_no;
    }

    public String getLang_id() {
        return lang_id;
    }

    public void setLang_id(String lang_id) {
        this.lang_id = lang_id;
    }

    public String getAdm_id() {
        return adm_id;
    }

    public void setAdm_id(String adm_id) {
        this.adm_id = adm_id;
    }

    public String getDt_fmt() {
        return dt_fmt;
    }

    public void setDt_fmt(String dt_fmt) {
        this.dt_fmt = dt_fmt;
    }

    public String getAmt_fmt_cd() {
        return amt_fmt_cd;
    }

    public void setAmt_fmt_cd(String amt_fmt_cd) {
        this.amt_fmt_cd = amt_fmt_cd;
    }

    public String getUsr_int_cd() {
        return usr_int_cd;
    }

    public void setUsr_int_cd(String usr_int_cd) {
        this.usr_int_cd = usr_int_cd;
    }

    public String getInc_rng_cd() {
        return inc_rng_cd;
    }

    public void setInc_rng_cd(String inc_rng_cd) {
        this.inc_rng_cd = inc_rng_cd;
    }

    public String getPrim_acid() {
        return prim_acid;
    }

    public void setPrim_acid(String prim_acid) {
        this.prim_acid = prim_acid;
    }

    public String getP_branch_id() {
        return p_branch_id;
    }

    public void setP_branch_id(String p_branch_id) {
        this.p_branch_id = p_branch_id;
    }

    public String getP_div_id() {
        return p_div_id;
    }

    public void setP_div_id(String p_div_id) {
        this.p_div_id = p_div_id;
    }

    public String getLoc_acc_ind() {
        return loc_acc_ind;
    }

    public void setLoc_acc_ind(String loc_acc_ind) {
        this.loc_acc_ind = loc_acc_ind;
    }

    public String getDiv_acc_ind() {
        return div_acc_ind;
    }

    public void setDiv_acc_ind(String div_acc_ind) {
        this.div_acc_ind = div_acc_ind;
    }

    public Timestamp getLast_txn_date() {
        return last_txn_date;
    }

    public void setLast_txn_date(Timestamp last_txn_date) {
        this.last_txn_date = last_txn_date;
    }

    public String getSms_pwd() {
        return sms_pwd;
    }

    public void setSms_pwd(String sms_pwd) {
        this.sms_pwd = sms_pwd;
    }

    public Integer getSms_no_of_atmpts() {
        return sms_no_of_atmpts;
    }

    public void setSms_no_of_atmpts(Integer sms_no_of_atmpts) {
        this.sms_no_of_atmpts = sms_no_of_atmpts;
    }

    public Timestamp getLogin_date() {
        return login_date;
    }

    public void setLogin_date(Timestamp login_date) {
        this.login_date = login_date;
    }

    public Timestamp getLogoff_date() {
        return logoff_date;
    }

    public void setLogoff_date(Timestamp logoff_date) {
        this.logoff_date = logoff_date;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getLogin_cert_match_reqd() {
        return login_cert_match_reqd;
    }

    public void setLogin_cert_match_reqd(String login_cert_match_reqd) {
        this.login_cert_match_reqd = login_cert_match_reqd;
    }

    public String getTxn_cert_match_reqd() {
        return txn_cert_match_reqd;
    }

    public void setTxn_cert_match_reqd(String txn_cert_match_reqd) {
        this.txn_cert_match_reqd = txn_cert_match_reqd;
    }

    public String getMulti_crn_txn_allowed() {
        return multi_crn_txn_allowed;
    }

    public void setMulti_crn_txn_allowed(String multi_crn_txn_allowed) {
        this.multi_crn_txn_allowed = multi_crn_txn_allowed;
    }

    public Double getAc_min_bal() {
        return ac_min_bal;
    }

    public void setAc_min_bal(Double ac_min_bal) {
        this.ac_min_bal = ac_min_bal;
    }

    public Double getTxn_limit() {
        return txn_limit;
    }

    public void setTxn_limit(Double txn_limit) {
        this.txn_limit = txn_limit;
    }

    public String getTxn_lmt_currency() {
        return txn_lmt_currency;
    }

    public void setTxn_lmt_currency(String txn_lmt_currency) {
        this.txn_lmt_currency = txn_lmt_currency;
    }

    public String getSms_alert() {
        return sms_alert;
    }

    public void setSms_alert(String sms_alert) {
        this.sms_alert = sms_alert;
    }

    public String getExt_alert() {
        return ext_alert;
    }

    public void setExt_alert(String ext_alert) {
        this.ext_alert = ext_alert;
    }

    public String getDefault_approver() {
        return default_approver;
    }

    public void setDefault_approver(String default_approver) {
        this.default_approver = default_approver;
    }

    public String getCorrespondence_address() {
        return correspondence_address;
    }

    public void setCorrespondence_address(String correspondence_address) {
        this.correspondence_address = correspondence_address;
    }

    public Integer getTot_num_login() {
        return tot_num_login;
    }

    public void setTot_num_login(Integer tot_num_login) {
        this.tot_num_login = tot_num_login;
    }

    public String getAvail_lang() {
        return avail_lang;
    }

    public void setAvail_lang(String avail_lang) {
        this.avail_lang = avail_lang;
    }

    public Timestamp getAc_opn_dt() {
        return ac_opn_dt;
    }

    public void setAc_opn_dt(Timestamp ac_opn_dt) {
        this.ac_opn_dt = ac_opn_dt;
    }

    public String getC_gender() {
        return c_gender;
    }

    public void setC_gender(String c_gender) {
        this.c_gender = c_gender;
    }

    public Timestamp getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Timestamp date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public Timestamp getAnniversary_date() {
        return anniversary_date;
    }

    public void setAnniversary_date(Timestamp anniversary_date) {
        this.anniversary_date = anniversary_date;
    }

    public String getNum_household() {
        return num_household;
    }

    public void setNum_household(String num_household) {
        this.num_household = num_household;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCategory_code() {
        return category_code;
    }

    public void setCategory_code(String category_code) {
        this.category_code = category_code;
    }

    public String getLimit_scheme() {
        return limit_scheme;
    }

    public void setLimit_scheme(String limit_scheme) {
        this.limit_scheme = limit_scheme;
    }

    public String getEducational_level() {
        return educational_level;
    }

    public void setEducational_level(String educational_level) {
        this.educational_level = educational_level;
    }

    public String getPassport_number() {
        return passport_number;
    }

    public void setPassport_number(String passport_number) {
        this.passport_number = passport_number;
    }

    public Timestamp getPassport_issue_date() {
        return passport_issue_date;
    }

    public void setPassport_issue_date(Timestamp passport_issue_date) {
        this.passport_issue_date = passport_issue_date;
    }

    public String getPassport_details() {
        return passport_details;
    }

    public void setPassport_details(String passport_details) {
        this.passport_details = passport_details;
    }

    public Timestamp getPassport_expiry_date() {
        return passport_expiry_date;
    }

    public void setPassport_expiry_date(Timestamp passport_expiry_date) {
        this.passport_expiry_date = passport_expiry_date;
    }

    public String getPan_national_id() {
        return pan_national_id;
    }

    public void setPan_national_id(String pan_national_id) {
        this.pan_national_id = pan_national_id;
    }

    public String getCust_status() {
        return cust_status;
    }

    public void setCust_status(String cust_status) {
        this.cust_status = cust_status;
    }

    public String getC_res_status() {
        return c_res_status;
    }

    public void setC_res_status(String c_res_status) {
        this.c_res_status = c_res_status;
    }

    public String getOnline_reg_pwd() {
        return online_reg_pwd;
    }

    public void setOnline_reg_pwd(String online_reg_pwd) {
        this.online_reg_pwd = online_reg_pwd;
    }

    public String getFree_text_1() {
        return free_text_1;
    }

    public void setFree_text_1(String free_text_1) {
        this.free_text_1 = free_text_1;
    }

    public String getFree_text_2() {
        return free_text_2;
    }

    public void setFree_text_2(String free_text_2) {
        this.free_text_2 = free_text_2;
    }

    public String getFree_text_3() {
        return free_text_3;
    }

    public void setFree_text_3(String free_text_3) {
        this.free_text_3 = free_text_3;
    }

    public String getFree_text_4() {
        return free_text_4;
    }

    public void setFree_text_4(String free_text_4) {
        this.free_text_4 = free_text_4;
    }

    public String getOof_flg() {
        return oof_flg;
    }

    public void setOof_flg(String oof_flg) {
        this.oof_flg = oof_flg;
    }

    public String getDel_flg() {
        return del_flg;
    }

    public void setDel_flg(String del_flg) {
        this.del_flg = del_flg;
    }

    public String getR_mod_id() {
        return r_mod_id;
    }

    public void setR_mod_id(String r_mod_id) {
        this.r_mod_id = r_mod_id;
    }

    public Timestamp getR_mod_time() {
        return r_mod_time;
    }

    public void setR_mod_time(Timestamp r_mod_time) {
        this.r_mod_time = r_mod_time;
    }

    public String getR_cre_id() {
        return r_cre_id;
    }

    public void setR_cre_id(String r_cre_id) {
        this.r_cre_id = r_cre_id;
    }

    public Timestamp getR_cre_time() {
        return r_cre_time;
    }

    public void setR_cre_time(Timestamp r_cre_time) {
        this.r_cre_time = r_cre_time;
    }

    public String getPage_scheme() {
        return page_scheme;
    }

    public void setPage_scheme(String page_scheme) {
        this.page_scheme = page_scheme;
    }

    public String getAuthentication_mode() {
        return authentication_mode;
    }

    public void setAuthentication_mode(String authentication_mode) {
        this.authentication_mode = authentication_mode;
    }

    public String getAuthorization_mode() {
        return authorization_mode;
    }

    public void setAuthorization_mode(String authorization_mode) {
        this.authorization_mode = authorization_mode;
    }

    public String getUser_id_type() {
        return user_id_type;
    }

    public void setUser_id_type(String user_id_type) {
        this.user_id_type = user_id_type;
    }

    public String getRet_auth_mode_prec() {
        return ret_auth_mode_prec;
    }

    public void setRet_auth_mode_prec(String ret_auth_mode_prec) {
        this.ret_auth_mode_prec = ret_auth_mode_prec;
    }

    public String getLogin_allowed() {
        return login_allowed;
    }

    public void setLogin_allowed(String login_allowed) {
        this.login_allowed = login_allowed;
    }

    public String getTransaction_allowed() {
        return transaction_allowed;
    }

    public void setTransaction_allowed(String transaction_allowed) {
        this.transaction_allowed = transaction_allowed;
    }

    public String getAuth_user() {
        return auth_user;
    }

    public void setAuth_user(String auth_user) {
        this.auth_user = auth_user;
    }

    public String getRange_limit_scheme() {
        return range_limit_scheme;
    }

    public void setRange_limit_scheme(String range_limit_scheme) {
        this.range_limit_scheme = range_limit_scheme;
    }

    public String getWf_rule_select_auth() {
        return wf_rule_select_auth;
    }

    public void setWf_rule_select_auth(String wf_rule_select_auth) {
        this.wf_rule_select_auth = wf_rule_select_auth;
    }

    public Timestamp getLast_unsuccessful_login_time() {
        return last_unsuccessful_login_time;
    }

    public void setLast_unsuccessful_login_time(Timestamp last_unsuccessful_login_time) {
        this.last_unsuccessful_login_time = last_unsuccessful_login_time;
    }

    public String getUser_encryption_key() {
        return user_encryption_key;
    }

    public void setUser_encryption_key(String user_encryption_key) {
        this.user_encryption_key = user_encryption_key;
    }

    public String getForce_terms_flag() {
        return force_terms_flag;
    }

    public void setForce_terms_flag(String force_terms_flag) {
        this.force_terms_flag = force_terms_flag;
    }

    public String getUser_txn_types() {
        return user_txn_types;
    }

    public void setUser_txn_types(String user_txn_types) {
        this.user_txn_types = user_txn_types;
    }

    public String getUser_alert_registration_flag() {
        return user_alert_registration_flag;
    }

    public void setUser_alert_registration_flag(String user_alert_registration_flag) {
        this.user_alert_registration_flag = user_alert_registration_flag;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public String getAlert_cust_id() {
        return alert_cust_id;
    }

    public void setAlert_cust_id(String alert_cust_id) {
        this.alert_cust_id = alert_cust_id;
    }

    public String getConcur_access_flg() {
        return concur_access_flg;
    }

    public void setConcur_access_flg(String concur_access_flg) {
        this.concur_access_flg = concur_access_flg;
    }

    public String getVirtual_user_flg() {
        return virtual_user_flg;
    }

    public void setVirtual_user_flg(String virtual_user_flg) {
        this.virtual_user_flg = virtual_user_flg;
    }

    public String getTran_auth_scheme() {
        return tran_auth_scheme;
    }

    public void setTran_auth_scheme(String tran_auth_scheme) {
        this.tran_auth_scheme = tran_auth_scheme;
    }

    public Integer getCp_entity_counter() {
        return cp_entity_counter;
    }

    public void setCp_entity_counter(Integer cp_entity_counter) {
        this.cp_entity_counter = cp_entity_counter;
    }

    public String getIndividual_id() {
        return individual_id;
    }

    public void setIndividual_id(String individual_id) {
        this.individual_id = individual_id;
    }

    public String getAcc_view_ind() {
        return acc_view_ind;
    }

    public void setAcc_view_ind(String acc_view_ind) {
        this.acc_view_ind = acc_view_ind;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getCust_asst_menu_prf() {
        return cust_asst_menu_prf;
    }

    public void setCust_asst_menu_prf(String cust_asst_menu_prf) {
        this.cust_asst_menu_prf = cust_asst_menu_prf;
    }

    public String getCal_type() {
        return cal_type;
    }

    public void setCal_type(String cal_type) {
        this.cal_type = cal_type;
    }

    public String getAcc_fmt() {
        return acc_fmt;
    }

    public void setAcc_fmt(String acc_fmt) {
        this.acc_fmt = acc_fmt;
    }

    public String getSegment_name() {
        return segment_name;
    }

    public void setSegment_name(String segment_name) {
        this.segment_name = segment_name;
    }

    public String getAccess_scheme() {
        return access_scheme;
    }

    public void setAccess_scheme(String access_scheme) {
        this.access_scheme = access_scheme;
    }

    public String getAccount_masking_required_flag() {
        return account_masking_required_flag;
    }

    public void setAccount_masking_required_flag(String account_masking_required_flag) {
        this.account_masking_required_flag = account_masking_required_flag;
    }

    public String getLogin_channel() {
        return login_channel;
    }

    public void setLogin_channel(String login_channel) {
        this.login_channel = login_channel;
    }

    public String getLogoff_channel() {
        return logoff_channel;
    }

    public void setLogoff_channel(String logoff_channel) {
        this.logoff_channel = logoff_channel;
    }

    public String getLast_unsuccessful_lgn_channel() {
        return last_unsuccessful_lgn_channel;
    }

    public void setLast_unsuccessful_lgn_channel(String last_unsuccessful_lgn_channel) {
        this.last_unsuccessful_lgn_channel = last_unsuccessful_lgn_channel;
    }

    public String getConfidential_txn_enabled() {
        return confidential_txn_enabled;
    }

    public void setConfidential_txn_enabled(String confidential_txn_enabled) {
        this.confidential_txn_enabled = confidential_txn_enabled;
    }

    public String getPrelogin_enabled_flag() {
        return prelogin_enabled_flag;
    }

    public void setPrelogin_enabled_flag(String prelogin_enabled_flag) {
        this.prelogin_enabled_flag = prelogin_enabled_flag;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "dbTs=" + dbTs +
                ", bankId='" + bankId + '\'' +
                ", orgId='" + orgId + '\'' +
                ", userId='" + userId + '\'' +
                ", userType='" + userType + '\'' +
                ", alertUserCategory='" + alertUserCategory + '\'' +
                ", customerId='" + customerId + '\'' +
                ", salutation='" + salutation + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", langCLName='" + langCLName + '\'' +
                ", langCMName='" + langCMName + '\'' +
                ", langCFName='" + langCFName + '\'' +
                ", emailId='" + emailId + '\'' +
                ", addres1='" + addres1 + '\'' +
                ", addres2='" + addres2 + '\'' +
                ", addres3='" + addres3 + '\'' +
                ", langCaddr1='" + langCaddr1 + '\'' +
                ", langCaddr2='" + langCaddr2 + '\'' +
                ", langCaddr3='" + langCaddr3 + '\'' +
                ", langCcity='" + langCcity + '\'' +
                ", c_state='" + c_state + '\'' +
                ", country='" + country + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", c_phone_no='" + c_phone_no + '\'' +
                ", mobilePhoneNumber='" + mobilePhoneNumber + '\'' +
                ", ddt_from=" + ddt_from +
                ", ddt_to=" + ddt_to +
                ", c_fax_no='" + c_fax_no + '\'' +
                ", lang_id='" + lang_id + '\'' +
                ", adm_id='" + adm_id + '\'' +
                ", dt_fmt='" + dt_fmt + '\'' +
                ", amt_fmt_cd='" + amt_fmt_cd + '\'' +
                ", usr_int_cd='" + usr_int_cd + '\'' +
                ", inc_rng_cd='" + inc_rng_cd + '\'' +
                ", prim_acid='" + prim_acid + '\'' +
                ", p_branch_id='" + p_branch_id + '\'' +
                ", p_div_id='" + p_div_id + '\'' +
                ", loc_acc_ind='" + loc_acc_ind + '\'' +
                ", div_acc_ind='" + div_acc_ind + '\'' +
                ", last_txn_date=" + last_txn_date +
                ", sms_pwd='" + sms_pwd + '\'' +
                ", sms_no_of_atmpts=" + sms_no_of_atmpts +
                ", login_date=" + login_date +
                ", logoff_date=" + logoff_date +
                ", session_id='" + session_id + '\'' +
                ", login_cert_match_reqd='" + login_cert_match_reqd + '\'' +
                ", txn_cert_match_reqd='" + txn_cert_match_reqd + '\'' +
                ", multi_crn_txn_allowed='" + multi_crn_txn_allowed + '\'' +
                ", ac_min_bal=" + ac_min_bal +
                ", txn_limit=" + txn_limit +
                ", txn_lmt_currency='" + txn_lmt_currency + '\'' +
                ", sms_alert='" + sms_alert + '\'' +
                ", ext_alert='" + ext_alert + '\'' +
                ", default_approver='" + default_approver + '\'' +
                ", correspondence_address='" + correspondence_address + '\'' +
                ", tot_num_login=" + tot_num_login +
                ", avail_lang='" + avail_lang + '\'' +
                ", ac_opn_dt=" + ac_opn_dt +
                ", c_gender='" + c_gender + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", marital_status='" + marital_status + '\'' +
                ", anniversary_date=" + anniversary_date +
                ", num_household='" + num_household + '\'' +
                ", occupation='" + occupation + '\'' +
                ", category_code='" + category_code + '\'' +
                ", limit_scheme='" + limit_scheme + '\'' +
                ", educational_level='" + educational_level + '\'' +
                ", passport_number='" + passport_number + '\'' +
                ", passport_issue_date=" + passport_issue_date +
                ", passport_details='" + passport_details + '\'' +
                ", passport_expiry_date=" + passport_expiry_date +
                ", pan_national_id='" + pan_national_id + '\'' +
                ", cust_status='" + cust_status + '\'' +
                ", c_res_status='" + c_res_status + '\'' +
                ", online_reg_pwd='" + online_reg_pwd + '\'' +
                ", free_text_1='" + free_text_1 + '\'' +
                ", free_text_2='" + free_text_2 + '\'' +
                ", free_text_3='" + free_text_3 + '\'' +
                ", free_text_4='" + free_text_4 + '\'' +
                ", oof_flg='" + oof_flg + '\'' +
                ", del_flg='" + del_flg + '\'' +
                ", r_mod_id='" + r_mod_id + '\'' +
                ", r_mod_time=" + r_mod_time +
                ", r_cre_id='" + r_cre_id + '\'' +
                ", r_cre_time=" + r_cre_time +
                ", page_scheme='" + page_scheme + '\'' +
                ", authentication_mode='" + authentication_mode + '\'' +
                ", authorization_mode='" + authorization_mode + '\'' +
                ", user_id_type='" + user_id_type + '\'' +
                ", ret_auth_mode_prec='" + ret_auth_mode_prec + '\'' +
                ", login_allowed='" + login_allowed + '\'' +
                ", transaction_allowed='" + transaction_allowed + '\'' +
                ", auth_user='" + auth_user + '\'' +
                ", range_limit_scheme='" + range_limit_scheme + '\'' +
                ", wf_rule_select_auth='" + wf_rule_select_auth + '\'' +
                ", last_unsuccessful_login_time=" + last_unsuccessful_login_time +
                ", user_encryption_key='" + user_encryption_key + '\'' +
                ", force_terms_flag='" + force_terms_flag + '\'' +
                ", user_txn_types='" + user_txn_types + '\'' +
                ", user_alert_registration_flag='" + user_alert_registration_flag + '\'' +
                ", scheme_id='" + scheme_id + '\'' +
                ", alert_cust_id='" + alert_cust_id + '\'' +
                ", concur_access_flg='" + concur_access_flg + '\'' +
                ", virtual_user_flg='" + virtual_user_flg + '\'' +
                ", tran_auth_scheme='" + tran_auth_scheme + '\'' +
                ", cp_entity_counter=" + cp_entity_counter +
                ", individual_id='" + individual_id + '\'' +
                ", acc_view_ind='" + acc_view_ind + '\'' +
                ", nick_name='" + nick_name + '\'' +
                ", cust_asst_menu_prf='" + cust_asst_menu_prf + '\'' +
                ", cal_type='" + cal_type + '\'' +
                ", acc_fmt='" + acc_fmt + '\'' +
                ", segment_name='" + segment_name + '\'' +
                ", access_scheme='" + access_scheme + '\'' +
                ", account_masking_required_flag='" + account_masking_required_flag + '\'' +
                ", login_channel='" + login_channel + '\'' +
                ", logoff_channel='" + logoff_channel + '\'' +
                ", last_unsuccessful_lgn_channel='" + last_unsuccessful_lgn_channel + '\'' +
                ", confidential_txn_enabled='" + confidential_txn_enabled + '\'' +
                ", prelogin_enabled_flag='" + prelogin_enabled_flag + '\'' +
                '}';
    }
}
