package id.co.bri.corp.model.common.user;

import id.co.bri.corp.model.common.types.ApprovalState;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="USER_PROFILE")
public class UserProfile implements Serializable{

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="TYPE", length=15, unique=true, nullable=false)
	private String type;

	@Column(name="code")
	private String code;

	@Column(name="ROLE")
	private String role;

	@Column(name="STATUS")
	private String state;

	@Column(name="APPROVAL_STATE")
	private String approvalState= ApprovalState.UNDEFINE.getState();

	@Column(name="LAST_MAKER_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date latestMakerDate;

	@Column(name="LAST_MAKER")
	private String latestMaker;

	@Column(name="LAST_APPROVER_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date latestApproverDate;

	@Column(name="LAST_APPROVER")
	private String latestApprover;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserProfile))
			return false;
		UserProfile other = (UserProfile) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getApprovalState() {
		return approvalState;
	}

	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}

	public Date getLatestMakerDate() {
		return latestMakerDate;
	}

	public void setLatestMakerDate(Date latestMakerDate) {
		this.latestMakerDate = latestMakerDate;
	}

	public String getLatestMaker() {
		return latestMaker;
	}

	public void setLatestMaker(String latestMaker) {
		this.latestMaker = latestMaker;
	}

	public Date getLatestApproverDate() {
		return latestApproverDate;
	}

	public void setLatestApproverDate(Date latestApproverDate) {
		this.latestApproverDate = latestApproverDate;
	}

	public String getLatestApprover() {
		return latestApprover;
	}

	public void setLatestApprover(String latestApprover) {
		this.latestApprover = latestApprover;
	}

	@Override
	public String toString() {
		return "UserProfile [id=" + id + ", type=" + type + "]";
	}

}
