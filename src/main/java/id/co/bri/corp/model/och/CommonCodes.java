package id.co.bri.corp.model.och;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "commoncodes")
public class CommonCodes implements Serializable {
    @Column(name = "code_type")
    protected String code_type;

    @Id
    @Column(name = "cm_code")
    protected String cm_code;

    @Column(name = "cd_desc")
    protected String cd_desc;

    public String getCode_type() {
        return code_type;
    }

    public void setCode_type(String code_type) {
        this.code_type = code_type;
    }

    public String getCd_desc() {
        return cd_desc;
    }

    public void setCd_desc(String cd_desc) {
        this.cd_desc = cd_desc;
    }

    @Override
    public String toString() {
        return "CommonCodes{" +
                "code_type='" + code_type + '\'' +
                ", cd_desc='" + cd_desc + '\'' +
                '}';
    }
}
