package id.co.bri.corp.model.common.user;

import id.co.bri.corp.model.common.types.ApprovalState;
import id.co.bri.corp.model.common.types.State;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "APP_USER")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @NotEmpty
    @Column(name = "SSO_ID", unique = true, nullable = false)
    private String ssoId;

    @NotEmpty
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "STATE", nullable = false)
    private String state = State.ACTIVE.getState();

    @Column(name = "APPROVAL_STATE")
    private String approvalState = ApprovalState.UNDEFINE.getState();

    @Column(name = "LOCATION", nullable = false)
    private String location = "Headquarters";

    @Column(name = "MOBILE_NO", nullable = false)
    private String mobileNo;

    @Column(name = "LAST_LOGIN")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLogin;

    @Column(name = "LAST_MAKER_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date latestMakerDate;

    @Column(name = "LAST_MAKER")
    private String latestMaker;

    @Column(name = "LAST_APPROVER_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date latestApproverDate;

    @Column(name = "LAST_APPROVER")
    private String latestApprover;

    @NotEmpty
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "APP_USER_USER_PROFILE",
            joinColumns = {@JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_PROFILE_ID")})
    private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((ssoId == null) ? 0 : ssoId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof User))
            return false;
        User other = (User) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (ssoId == null) {
            if (other.ssoId != null)
                return false;
        } else if (!ssoId.equals(other.ssoId))
            return false;
        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getApprovalState() {
        return approvalState;
    }

    public void setApprovalState(String approvalState) {
        this.approvalState = approvalState;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getLatestMakerDate() {
        return latestMakerDate;
    }

    public void setLatestMakerDate(Date latestMakerDate) {
        this.latestMakerDate = latestMakerDate;
    }

    public String getLatestMaker() {
        return latestMaker;
    }

    public void setLatestMaker(String latestMaker) {
        this.latestMaker = latestMaker;
    }

    public Date getLatestApproverDate() {
        return latestApproverDate;
    }

    public void setLatestApproverDate(Date latestApproverDate) {
        this.latestApproverDate = latestApproverDate;
    }

    public String getLatestApprover() {
        return latestApprover;
    }

    public void setLatestApprover(String latestApprover) {
        this.latestApprover = latestApprover;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", ssoId='" + ssoId + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", state='" + state + '\'' +
                ", approvalState='" + approvalState + '\'' +
                ", location='" + location + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", lastLogin=" + lastLogin +
                ", latestMakerDate=" + latestMakerDate +
                ", latestMaker='" + latestMaker + '\'' +
                ", latestApproverDate=" + latestApproverDate +
                ", latestApprover='" + latestApprover + '\'' +
                ", userProfiles=" + userProfiles +
                '}';
    }
}
