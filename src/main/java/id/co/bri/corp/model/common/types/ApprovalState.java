package id.co.bri.corp.model.common.types;


public enum ApprovalState {

	PENDING("Pending"),
	APPROVED("Approved"),
	REJECTED("Rejected"),
	UNDEFINE("");

	private String state;

	ApprovalState(final String state){
		this.state = state;
	}
	
	public String getState(){
		return this.state;
	}

	@Override
	public String toString(){
		return this.state;
	}

	public String getName(){
		return this.name();
	}


}
