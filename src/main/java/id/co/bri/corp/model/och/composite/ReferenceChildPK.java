package id.co.bri.corp.model.och.composite;


import java.io.Serializable;

public class ReferenceChildPK implements Serializable {
    protected String code;
    protected String name;

    public ReferenceChildPK() {
    }

    public ReferenceChildPK(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
