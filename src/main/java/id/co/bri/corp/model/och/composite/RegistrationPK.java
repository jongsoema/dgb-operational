package id.co.bri.corp.model.och.composite;


import java.io.Serializable;

public class RegistrationPK implements Serializable {
    protected String bankId;
    protected String customerId;

    public RegistrationPK() {
    }

    public RegistrationPK(String bankId, String customerId) {
        this.bankId = bankId;
        this.customerId = customerId;
    }
}
