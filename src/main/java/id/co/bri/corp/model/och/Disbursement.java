package id.co.bri.corp.model.och;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import id.co.bri.corp.model.core.FundTransfer;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
@Table(name = "custom_loan_appln_master_table")
public class Disbursement implements Serializable {
    @Id
    @Column(name = "application_id")
    protected Integer application_id;

    @Column(name = "db_ts")
    protected Integer dbTs;

    @Column(name = "bank_id")
    protected String bank_id;

    @Column(name = "user_id")
    protected String user_id;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "application_id",insertable=false, updatable=false)
    protected ApplicationDetail applicationDetail;

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "application_id",insertable=false, updatable=false)
    protected PayrollDetail payrollDetail;

    @Column(name = "requested_loan_amt")
    protected Double requested_loan_amt;

    @Column(name = "requested_tenor")
    protected Integer requested_tenor;

    @Column(name = "loan_int_rate")
    protected Double loan_int_rate;

    @Column(name = "monhtly_installment")
    protected Double monhtly_installment;

    @Column(name = "legacy_cif")
    protected String legacy_cif;

    @JsonIgnore
    @Column(name = "privy_agree_date")
    protected Timestamp privy_agree_date;

    @JsonIgnore
    @Column(name = "product_tnc_agree_date")
    protected Timestamp product_tnc_agree_date;

    @Column(name = "application_status")
    protected String application_status;

    @Column(name = "product_type")
    protected String product_type;

    @Column(name = "product_code")
    protected String product_code;

    @Column(name = "product_category")
    protected String product_category;

    @Column(name = "product_subcategory")
    protected String product_subcategory;

    @Column(name = "free_txt1")
    protected String free_txt1;

    @Column(name = "free_txt2")
    protected String free_txt2;

    @Column(name = "free_txt3")
    protected String free_txt3;

    @JsonIgnore
    @Column(name = "free_date1")
    protected Timestamp free_date1;

    @JsonIgnore
    @Column(name = "free_date2")
    protected Timestamp free_date2;

    @Column(name = "del_flg")
    protected String del_flg;

    @Column(name = "r_mod_id")
    protected String r_mod_id;

    @JsonIgnore
    @Column(name = "r_mod_time")
    protected Timestamp r_mod_time;

    @Column(name = "r_cre_id")
    protected String r_cre_id;

    @JsonIgnore
    @Column(name = "r_cre_time")
    protected Timestamp r_cre_time;

    @Column(name = "email_verified")
    protected String email_verified;

    @Column(name = "credit_score_uid")
    protected String credit_score_uid;

    @Column(name = "credit_score")
    protected Double credit_score;

    @Column(name = "grade")
    protected Integer grade;

    @Column(name = "approved_amount")
    protected Double approved_amount;

    @Column(name = "status_desc")
    protected String status_desc;

    @Column(name = "loan_account_id")
    protected String loan_account_id;

    @Column(name = "feedback_star")
    protected String feedback_star;

    @Column(name = "remarks")
    protected String remarks;


    public Integer getDbTs() {
        return dbTs;
    }

    public void setDbTs(Integer dbTs) {
        this.dbTs = dbTs;
    }

    public String getBank_id() {
        return bank_id;
    }

    public void setBank_id(String bank_id) {
        this.bank_id = bank_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ApplicationDetail getApplicationDetail() {
        return applicationDetail;
    }

    public void setApplicationDetail(ApplicationDetail applicationDetail) {
        this.applicationDetail = applicationDetail;
    }

    public Integer getApplication_id() {
        return application_id;
    }

    public PayrollDetail getPayrollDetail() {
        return payrollDetail;
    }

    public void setPayrollDetail(PayrollDetail payrollDetail) {
        this.payrollDetail = payrollDetail;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public Double getRequested_loan_amt() {
        return requested_loan_amt;
    }

    public void setRequested_loan_amt(Double requested_loan_amt) {
        this.requested_loan_amt = requested_loan_amt;
    }

    public Integer getRequested_tenor() {
        return requested_tenor;
    }

    public void setRequested_tenor(Integer requested_tenor) {
        this.requested_tenor = requested_tenor;
    }

    public Double getLoan_int_rate() {
        return loan_int_rate;
    }

    public void setLoan_int_rate(Double loan_int_rate) {
        this.loan_int_rate = loan_int_rate;
    }

    public Double getMonhtly_installment() {
        return monhtly_installment;
    }

    public void setMonhtly_installment(Double monhtly_installment) {
        this.monhtly_installment = monhtly_installment;
    }

    public String getLegacy_cif() {
        return legacy_cif;
    }

    public void setLegacy_cif(String legacy_cif) {
        this.legacy_cif = legacy_cif;
    }

    public Timestamp getPrivy_agree_date() {
        return privy_agree_date;
    }

    public void setPrivy_agree_date(Timestamp privy_agree_date) {
        this.privy_agree_date = privy_agree_date;
    }

    public Timestamp getProduct_tnc_agree_date() {
        return product_tnc_agree_date;
    }

    public void setProduct_tnc_agree_date(Timestamp product_tnc_agree_date) {
        this.product_tnc_agree_date = product_tnc_agree_date;
    }

    public String getApplication_status() {
        return application_status;
    }

    public void setApplication_status(String application_status) {
        this.application_status = application_status;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getProduct_subcategory() {
        return product_subcategory;
    }

    public void setProduct_subcategory(String product_subcategory) {
        this.product_subcategory = product_subcategory;
    }

    public String getFree_txt1() {
        return free_txt1;
    }

    public void setFree_txt1(String free_txt1) {
        this.free_txt1 = free_txt1;
    }

    public String getFree_txt2() {
        return free_txt2;
    }

    public void setFree_txt2(String free_txt2) {
        this.free_txt2 = free_txt2;
    }

    public String getFree_txt3() {
        return free_txt3;
    }

    public void setFree_txt3(String free_txt3) {
        this.free_txt3 = free_txt3;
    }

    public Timestamp getFree_date1() {
        return free_date1;
    }

    public void setFree_date1(Timestamp free_date1) {
        this.free_date1 = free_date1;
    }

    public Timestamp getFree_date2() {
        return free_date2;
    }

    public void setFree_date2(Timestamp free_date2) {
        this.free_date2 = free_date2;
    }

    public String getDel_flg() {
        return del_flg;
    }

    public void setDel_flg(String del_flg) {
        this.del_flg = del_flg;
    }

    public String getR_mod_id() {
        return r_mod_id;
    }

    public void setR_mod_id(String r_mod_id) {
        this.r_mod_id = r_mod_id;
    }

    public Timestamp getR_mod_time() {
        return r_mod_time;
    }

    public void setR_mod_time(Timestamp r_mod_time) {
        this.r_mod_time = r_mod_time;
    }

    public String getR_cre_id() {
        return r_cre_id;
    }

    public void setR_cre_id(String r_cre_id) {
        this.r_cre_id = r_cre_id;
    }

    public Timestamp getR_cre_time() {
        return r_cre_time;
    }

    public void setR_cre_time(Timestamp r_cre_time) {
        this.r_cre_time = r_cre_time;
    }

    public String getEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(String email_verified) {
        this.email_verified = email_verified;
    }

    public String getCredit_score_uid() {
        return credit_score_uid;
    }

    public void setCredit_score_uid(String credit_score_uid) {
        this.credit_score_uid = credit_score_uid;
    }

    public Double getCredit_score() {
        return credit_score;
    }

    public void setCredit_score(Double credit_score) {
        this.credit_score = credit_score;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Double getApproved_amount() {
        return approved_amount;
    }

    public void setApproved_amount(Double approved_amount) {
        this.approved_amount = approved_amount;
    }

    public String getStatus_desc() {
        return status_desc;
    }

    public void setStatus_desc(String status_desc) {
        this.status_desc = status_desc;
    }

    public String getLoan_account_id() {
        return loan_account_id;
    }

    public void setLoan_account_id(String loan_account_id) {
        this.loan_account_id = loan_account_id;
    }

    public String getFeedback_star() {
        return feedback_star;
    }

    public void setFeedback_star(String feedback_star) {
        this.feedback_star = feedback_star;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
