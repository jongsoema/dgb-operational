package id.co.bri.corp.model.common.types;


public enum State {

	ACTIVE("Active"),
	INACTIVE("Inactive"),
	SUSPEND("Suspended"),
	DELETED("Deleted"),
	LOCKED("Locked");
	
	private String state;
	
	private State(final String state){
		this.state = state;
	}
	
	public String getState(){
		return this.state;
	}

	@Override
	public String toString(){
		return this.state;
	}

	public String getName(){
		return this.name();
	}


}
