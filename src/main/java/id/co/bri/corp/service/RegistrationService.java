package id.co.bri.corp.service;

import id.co.bri.corp.model.och.Registration;

import java.util.List;


public interface RegistrationService {
    Registration findById(String userId);
    List<Registration> getAllRegistrations();
}