package id.co.bri.corp.service;

import id.co.bri.corp.dao.PayrollDetailDao;
import id.co.bri.corp.model.och.PayrollDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("payrollDetailService")
@Transactional
public class PayrollDetailServiceImpl implements PayrollDetailService {
    @Autowired
    PayrollDetailDao payrollDetailDao;

    @Override
    public PayrollDetail findById(String applicationId) {
        return null;
    }

    @Override
    public List<PayrollDetail> getaAllPayrollAccount() {
        return payrollDetailDao.getaAllPayrollAccount();
    }
}
