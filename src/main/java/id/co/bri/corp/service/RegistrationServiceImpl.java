package id.co.bri.corp.service;

import id.co.bri.corp.dao.RegistrationDao;
import id.co.bri.corp.model.och.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("registrationService")
@Transactional
public class RegistrationServiceImpl implements RegistrationService {
    @Autowired
    RegistrationDao registrationDao;

    @Override
    public Registration findById(String userId) {
        return registrationDao.findById(userId);
    }

    @Override
    public List<Registration> getAllRegistrations() {
        return registrationDao.getAllRegistrations();
    }
}
