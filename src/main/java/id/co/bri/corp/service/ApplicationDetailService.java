package id.co.bri.corp.service;

import id.co.bri.corp.model.och.ApplicationDetail;


public interface ApplicationDetailService {
    ApplicationDetail findById(String userId);
}