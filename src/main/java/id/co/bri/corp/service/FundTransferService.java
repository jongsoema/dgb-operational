package id.co.bri.corp.service;

import id.co.bri.corp.model.common.form.BaseFilter;
import id.co.bri.corp.model.core.FundTransfer;
import id.co.bri.corp.model.och.Disbursement;

import java.util.List;


public interface FundTransferService {
    List<FundTransfer> getAgfFiltered(final BaseFilter baseFilter, Integer mode);
    List<FundTransfer> getAllAgfs();

    List<FundTransfer> getAllDisbursement();
    List<FundTransfer> getDisbursementFiltered(final BaseFilter baseFilter, Integer mode);
}