package id.co.bri.corp.service;

import java.util.Date;
import java.util.List;

import id.co.bri.corp.dao.UserDao;
import id.co.bri.corp.model.common.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao dao;

	public User findById(int id) {
		return dao.findById(id);
	}

	public User findBySSO(String sso) {
		User user = dao.findBySSO(sso);
		return user;
	}

	public void saveUser(User user) {
		dao.save(user);
	}

	public void updateUser(User user) {
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setSsoId(user.getSsoId());
			entity.setPassword(user.getPassword());
			entity.setEmail(user.getEmail());
			entity.setState(user.getState());
			entity.setApprovalState(user.getApprovalState());
			entity.setLocation(user.getLocation());
			entity.setMobileNo(user.getMobileNo());
			entity.setLastLogin(user.getLastLogin());
			entity.setUserProfiles(user.getUserProfiles());
			entity.setLatestMakerDate(new Date());
			entity.setLatestApproverDate(new Date());
		}
	}


	public void deleteUserBySSO(String sso) {
		dao.deleteBySSO(sso);
	}

	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	@Override
	public List<User> findAllPendingAuth() {
		return dao.findAllPendingAuth();
	}

	public boolean isUserSSOUnique(Integer id, String sso) {
		User user = findBySSO(sso);
		return ( user == null || ((id != null) && (user.getId() == id)));
	}
	
}
