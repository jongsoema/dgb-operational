package id.co.bri.corp.service;

import id.co.bri.corp.model.och.Disbursement;

import java.util.List;


public interface DisbursementService {
    Disbursement findById(String userId);
    List<Disbursement> getAllLoanCreated();
    List<Disbursement> getAllLoanPaid();
    List<Disbursement> getAllApplications();
    List<Disbursement> getAll();
}