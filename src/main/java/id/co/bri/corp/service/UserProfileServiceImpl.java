package id.co.bri.corp.service;

import id.co.bri.corp.dao.UserProfileDao;
import id.co.bri.corp.model.common.user.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    UserProfileDao dao;

    public UserProfile findById(int id) {
        return dao.findById(id);
    }

    public UserProfile findByType(String type) {
        return dao.findByType(type);
    }

    @Override
    public UserProfile findByRoleCode(String roleCode) {
        return dao.findByRoleCode(roleCode);
    }

    @Override
    public void saveUserProfile(final UserProfile userProfile) {
        dao.saveUserProfile(userProfile);
    }

    @Override
    public void updateUserProfile(UserProfile userProfile) {
        dao.updateUserProfile(userProfile);
    }

    public List<UserProfile> findAll() {
        return dao.findAll();
    }

    @Override
    public List<UserProfile> findAllPending() {
        return dao.findAllPending();
    }
}
