package id.co.bri.corp.service;

import id.co.bri.corp.dao.FundTransferDao;
import id.co.bri.corp.model.common.form.BaseFilter;
import id.co.bri.corp.model.core.FundTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("fundTransferService")
@Transactional
public class FundTransferServiceImpl implements FundTransferService {
    @Autowired
    FundTransferDao fundTransferDao;

    @Override
    public List<FundTransfer> getAgfFiltered(BaseFilter baseFilter,Integer mode) {
        return fundTransferDao.getAgfFiltered(baseFilter, mode);
    }

    @Override
    public List<FundTransfer> getAllAgfs() {
        return fundTransferDao.getAllAgfs();
    }

    @Override
    public List<FundTransfer> getAllDisbursement() {
        return fundTransferDao.getAllDisbursement();
    }

    @Override
    public List<FundTransfer> getDisbursementFiltered(BaseFilter baseFilter, Integer mode) {
        return fundTransferDao.getDisbursementFiltered(baseFilter, mode);
    }

}
