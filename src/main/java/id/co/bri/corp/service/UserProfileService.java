package id.co.bri.corp.service;

import id.co.bri.corp.model.common.user.UserProfile;

import java.util.List;


public interface UserProfileService {

    UserProfile findById(int id);

    UserProfile findByType(String type);

    UserProfile findByRoleCode(String roleCode);

    void saveUserProfile(final UserProfile userProfile);

    void updateUserProfile(UserProfile userProfile);

    List<UserProfile> findAll();

    List<UserProfile> findAllPending();

}
