package id.co.bri.corp.service;

import id.co.bri.corp.model.och.PayrollDetail;

import java.util.List;


public interface PayrollDetailService {
    PayrollDetail findById(String applicationId);

    List<PayrollDetail> getaAllPayrollAccount();
}