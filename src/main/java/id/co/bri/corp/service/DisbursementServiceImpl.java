package id.co.bri.corp.service;

import id.co.bri.corp.dao.DisbursementDao;
import id.co.bri.corp.model.och.Disbursement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("disbursementService")
@Transactional
public class DisbursementServiceImpl implements DisbursementService {
    @Autowired
    DisbursementDao disbursementDao;

    @Override
    public Disbursement findById(String userId) {
        return disbursementDao.findById(userId);
    }

    @Override
    public List<Disbursement> getAllLoanCreated() {
        return disbursementDao.getAllLoanCreated();
    }

    @Override
    public List<Disbursement> getAllLoanPaid() {
        return disbursementDao.getAllLoanPaid();
    }

    @Override
    public List<Disbursement> getAllApplications() {
        return disbursementDao.getAllApplications();
    }

    @Override
    public List<Disbursement> getAll() {
        return disbursementDao.getAll();
    }
}
