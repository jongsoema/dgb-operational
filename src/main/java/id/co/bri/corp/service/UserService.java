package id.co.bri.corp.service;

import id.co.bri.corp.model.common.user.User;

import java.util.List;


public interface UserService {

    User findById(int id);

    User findBySSO(String sso);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserBySSO(String sso);

    List<User> findAllUsers();

    List<User> findAllPendingAuth();

    boolean isUserSSOUnique(Integer id, String sso);

}