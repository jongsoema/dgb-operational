package id.co.bri.corp.service;

import id.co.bri.corp.dao.ApplicationDetailDao;
import id.co.bri.corp.model.och.ApplicationDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("applicationDetailService")
@Transactional
public class ApplicationDetailServiceImpl implements ApplicationDetailService {
    @Autowired
    ApplicationDetailDao applicationDetailDao;

    @Override
    public ApplicationDetail findById(String userId) {
        return applicationDetailDao.findById(userId);
    }
}
