package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.Registration;

import java.util.List;

public interface RegistrationDao {
    Registration findById(String userId);
    List<Registration> getAllRegistrations();
}
