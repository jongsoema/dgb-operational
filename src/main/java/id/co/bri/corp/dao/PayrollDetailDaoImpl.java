package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.PayrollDetail;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("payrollDetailDao")
public class PayrollDetailDaoImpl extends AbstractDaoCore<String, PayrollDetail> implements PayrollDetailDao {
    @Override
    public PayrollDetail findById(String applicationId) {
        return null;
    }

    @Override
    public List<PayrollDetail> getaAllPayrollAccount() {
        List<PayrollDetail> payrollDetails = getEntityManager()
                .createQuery("SELECT p FROM PayrollDetail p")
                .getResultList();
        return payrollDetails;
    }
}
