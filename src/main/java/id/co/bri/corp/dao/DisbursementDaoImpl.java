package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.Disbursement;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("disbursementDao")
public class DisbursementDaoImpl extends AbstractDao<String, Disbursement> implements DisbursementDao {
    @Override
    public Disbursement findById(String userId) {
        try {
            Disbursement registration = (Disbursement) getEntityManager()
                    .createQuery("SELECT p FROM Disbursement p WHERE p.userId LIKE :userId")
                    .setParameter("userId", userId)
                    .getSingleResult();
            return registration;
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public List<Disbursement> getAllLoanCreated() {
        List<Disbursement> disbursements = getEntityManager()
                .createQuery("SELECT p FROM Disbursement p WHERE p.application_status = :application_status OR p.application_status = :application_status2 ORDER BY p.r_cre_time DESC")
                .setParameter("application_status", "LOAN_CREATED")
                .setParameter("application_status2", "LOAN_PAID")
                .getResultList();
        return disbursements;
    }

    @Override
    public List<Disbursement> getAllLoanPaid() {
        List<Disbursement> disbursements = getEntityManager()
                .createQuery("SELECT p FROM Disbursement p WHERE p.application_status = :application_status ORDER BY p.r_cre_time DESC")
                .setParameter("application_status", "LOAN_PAID")
                .getResultList();
        System.out.println("##getAllLoanPaid==>" + disbursements.size());
        return disbursements;
    }

    @Override
    public List<Disbursement> getAll() {
        List<Disbursement> disbursements = getEntityManager()
                .createQuery("SELECT p FROM Disbursement p ORDER BY p.r_cre_time DESC")
                .getResultList();
        return disbursements;
    }

    @Override
    public List<Disbursement> getAllApplications() {
        List<Disbursement> disbursements = getEntityManager()
                .createQuery("SELECT p FROM Disbursement p WHERE (p.user_id, p.r_cre_time) in (select q.user_id, max(q.r_cre_time) FROM Disbursement q GROUP BY q.user_id) ORDER BY p.r_cre_time DESC")
                .getResultList();
        return disbursements;
    }
}