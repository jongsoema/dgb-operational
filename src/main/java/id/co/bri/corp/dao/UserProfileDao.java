package id.co.bri.corp.dao;

import id.co.bri.corp.model.common.user.UserProfile;

import java.util.List;


public interface UserProfileDao {

    List<UserProfile> findAll();

    List<UserProfile> findAllPending();

    UserProfile findByType(String type);

    UserProfile findById(int id);

    UserProfile findByRoleCode(String roleCode);

    void saveUserProfile(final UserProfile userProfile);

    void updateUserProfile(UserProfile userProfile);
}
