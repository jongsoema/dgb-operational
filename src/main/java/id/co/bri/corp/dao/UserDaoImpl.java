package id.co.bri.corp.dao;


import id.co.bri.corp.model.common.types.ApprovalState;
import id.co.bri.corp.model.common.user.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

    public User findById(int id) {
        User user = getByKey(id);
        if (user != null) {
            initializeCollection(user.getUserProfiles());
        }
        return user;
    }

    public User findBySSO(String sso) {
        try {
            User user = (User) getEntityManager()
                    .createQuery("SELECT u FROM User u WHERE u.ssoId LIKE :ssoId")
                    .setParameter("ssoId", sso)
                    .getSingleResult();

            if (user != null) {
                initializeCollection(user.getUserProfiles());
            }
            return user;
        } catch (NoResultException ex) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> findAllUsers() {
        Optional<List<User>> users = Optional.ofNullable(getEntityManager()
                .createQuery("SELECT u FROM User u ORDER BY u.id ASC")
                .getResultList());
        if (users.isPresent()) {
            users.get().stream().forEach(user -> {
                if (Objects.nonNull(user.getUserProfiles()) || user.getUserProfiles().size() > 0) {
                    initializeCollection(user.getUserProfiles());
                }
            });
        }
        return users.get();
    }

    @Override
    public List<User> findAllPendingAuth() {
        try {
            Optional<List<User>> users = Optional.ofNullable(getEntityManager()
                    .createQuery("SELECT u FROM User u WHERE u.approvalState = :state")
                    .setParameter("state", ApprovalState.PENDING.getState())
                    .getResultList());
            if (users.isPresent()) {
                users.get().stream().forEach(user -> {
                    if (Objects.nonNull(user.getUserProfiles()) || user.getUserProfiles().size() > 0) {
                        initializeCollection(user.getUserProfiles());
                    }
                });
            }
            return users.get();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public void save(User user) {
        persist(user);
    }

    public void deleteBySSO(String sso) {
        User user = (User) getEntityManager()
                .createQuery("SELECT u FROM User u WHERE u.ssoId LIKE :ssoId")
                .setParameter("ssoId", sso)
                .getSingleResult();
        delete(user);
    }

    //An alternative to Hibernate.initialize()
    protected void initializeCollection(Collection<?> collection) {
        if (collection == null) {
            return;
        }
        collection.iterator().hasNext();
    }

}
