package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.Disbursement;

import java.util.List;

public interface DisbursementDao {
    Disbursement findById(String userId);
    List<Disbursement> getAllLoanCreated();
    List<Disbursement> getAllLoanPaid();
    List<Disbursement> getAllApplications();
    List<Disbursement> getAll();
}
