package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.ApplicationDetail;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("applicationDetailDao")
public class ApplicationDetailDaoImpl extends AbstractDao<String, ApplicationDetail> implements ApplicationDetailDao {
    @Override
    public ApplicationDetail findById(String userId) {
        userId = userId.substring(1, userId.length());
        try {
            List<ApplicationDetail> applicationDetails = getEntityManager()
                    .createQuery("SELECT p FROM ApplicationDetail p WHERE p.mobile_no = :userId ORDER BY p.r_cre_time DESC")
                    .setParameter("userId", Long.valueOf(userId))
                    .getResultList();
            return applicationDetails.get(0);
        } catch (NoResultException ex) {
            return null;
        }
    }
}