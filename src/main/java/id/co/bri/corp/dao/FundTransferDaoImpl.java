package id.co.bri.corp.dao;

import id.co.bri.corp.model.common.form.BaseFilter;
import id.co.bri.corp.model.core.FundTransfer;
import id.co.bri.corp.model.och.Disbursement;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("fundTransferDao")
public class FundTransferDaoImpl extends AbstractDaoCore<String, FundTransfer> implements FundTransferDao {
    @Override
    public List<FundTransfer> getAllAgfs() {
        List<FundTransfer> fundTransfers = getEntityManager()
                .createQuery("SELECT p FROM FundTransfer p WHERE p.transfer_type='AutoPay' ORDER BY p.transaction_date_time DESC")
                .getResultList();
        return fundTransfers;
    }

    @Override
    public List<FundTransfer> getAgfFiltered(final BaseFilter baseFilter, Integer mode) {
        String status = "";
        switch (baseFilter.getStatus()) {
            case "SUCCESS" : status = "S";break;
            case "FAILED" : status = "F";break;
            case "PENDING" : status = "P";break;
        }
        if (mode == 1) {
            System.out.println("mode 1...");
            List<FundTransfer> fundTransfers = getEntityManager()
                    .createQuery("SELECT p FROM FundTransfer p WHERE p.status=:status AND p.loan_account=:loan_account AND p.transfer_type='AutoPay' AND (p.rcre_time BETWEEN :fromDate AND :toDate) ORDER BY p.transaction_date_time DESC")
                    .setParameter("loan_account", baseFilter.getLoanAccount())
                    .setParameter("fromDate", baseFilter.getFromDate())
                    .setParameter("toDate", baseFilter.getToDate())
                    .setParameter("status", status)
                    .getResultList();
            return fundTransfers;
        }
        else if (mode == 2) {
            List<FundTransfer> fundTransfers = getEntityManager()
                    .createQuery("SELECT p FROM FundTransfer p WHERE p.status=:status AND p.transfer_type='AutoPay' AND (p.rcre_time BETWEEN :fromDate AND :toDate) ORDER BY p.transaction_date_time DESC")
                    .setParameter("fromDate", baseFilter.getFromDate())
                    .setParameter("toDate", baseFilter.getToDate())
                    .setParameter("status", status)
                    .getResultList();
            return fundTransfers;
        }
        return null;
    }

    @Override
    public List<FundTransfer> getAllDisbursement() {
        List<FundTransfer> fundTransfers = getEntityManager()
                .createQuery("SELECT p FROM FundTransfer p WHERE p.transfer_type = :transfer_type ORDER BY p.transaction_date_time DESC")
                .setParameter("transfer_type", "DisbursementOut")
                .getResultList();
        return fundTransfers;
    }

    @Override
    public List<FundTransfer> getDisbursementFiltered(BaseFilter baseFilter, Integer mode) {
        String status = "";
        switch (baseFilter.getStatus()) {
            case "SUCCESS" : status = "S";break;
            case "PENDING" : status = "P";break;
            case "FAILED" : status = "F";break;
        }
        if (mode == 1) {
            List<FundTransfer> fundTransfers = getEntityManager()
                    .createQuery("SELECT p FROM FundTransfer p WHERE p.status=:status AND p.loan_account=:loan_account AND p.transfer_type='DisbursementOut' AND (p.rcre_time BETWEEN :fromDate AND :toDate) ORDER BY p.transaction_date_time DESC")
                    .setParameter("loan_account", baseFilter.getLoanAccount())
                    .setParameter("fromDate", baseFilter.getFromDate())
                    .setParameter("toDate", baseFilter.getToDate())
                    .setParameter("status", status)
                    .getResultList();
            return fundTransfers;
        }
        else if (mode == 2) {
            List<FundTransfer> fundTransfers = getEntityManager()
                    .createQuery("SELECT p FROM FundTransfer p WHERE p.status=:status AND p.transfer_type='DisbursementOut' AND (p.rcre_time BETWEEN :fromDate AND :toDate) ORDER BY p.transaction_date_time DESC")
                    .setParameter("fromDate", baseFilter.getFromDate())
                    .setParameter("toDate", baseFilter.getToDate())
                    .setParameter("status", status)
                    .getResultList();
            return fundTransfers;
        }
        return null;
    }
}
