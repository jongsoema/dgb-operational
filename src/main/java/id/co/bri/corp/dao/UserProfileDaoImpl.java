package id.co.bri.corp.dao;

import java.util.List;

import javax.persistence.NoResultException;

import id.co.bri.corp.model.common.types.ApprovalState;
import id.co.bri.corp.model.common.user.UserProfile;
import org.springframework.stereotype.Repository;



@Repository("userProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Integer, UserProfile>implements UserProfileDao{

	public UserProfile findById(int id) {
		return getByKey(id);
	}

	@Override
	public UserProfile findByRoleCode(String roleCode) {
		try{
			UserProfile userProfile = (UserProfile) getEntityManager()
					.createQuery("SELECT p FROM UserProfile p WHERE p.code LIKE :code")
					.setParameter("code", roleCode)
					.getSingleResult();
			return userProfile;
		}catch(NoResultException ex){
			return null;
		}
	}

	@Override
	public void saveUserProfile(final UserProfile userProfile) {
		persist(userProfile);
	}

	@Override
	public void updateUserProfile(UserProfile userProfile) {
		update(userProfile);
	}

	public UserProfile findByType(String type) {
		try {
			UserProfile userProfile = (UserProfile) getEntityManager()
					.createQuery("SELECT p FROM UserProfile p WHERE p.type LIKE :type")
					.setParameter("type", type)
					.getSingleResult();
			return userProfile;
		}catch(NoResultException ex){
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<UserProfile> findAll(){
		List<UserProfile> userProfiles = getEntityManager()
				.createQuery("SELECT p FROM UserProfile p  ORDER BY p.id ASC")
				.getResultList();
		return userProfiles;
	}

	@Override
	public List<UserProfile> findAllPending() {
		List<UserProfile> userProfiles = getEntityManager()
				.createQuery("SELECT p FROM UserProfile p  WHERE p.approvalState = :approvalState ORDER BY p.id ASC")
				.setParameter("approvalState", ApprovalState.PENDING.getState())
				.getResultList();
		return userProfiles;
	}

}
