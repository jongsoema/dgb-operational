package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.Registration;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository("registrationDao")
public class RegistrationDaoImpl extends AbstractDao<String, Registration> implements RegistrationDao {
    @Override
    public Registration findById(String userId) {
        try {
            Registration registration = (Registration) getEntityManager()
                    .createQuery("SELECT p FROM Registration p WHERE p.userId LIKE :userId")
                    .setParameter("userId", userId)
                    .getSingleResult();
            return registration;
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public List<Registration> getAllRegistrations() {
        List<Registration> registrationList = getEntityManager()
                .createQuery("SELECT p FROM Registration p where p.userType =:ty  ORDER BY p.r_mod_time DESC")
                .setParameter("ty", "1")
                .getResultList();
        return registrationList;
    }
}
