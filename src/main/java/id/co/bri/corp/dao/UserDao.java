package id.co.bri.corp.dao;

import java.util.List;

import id.co.bri.corp.model.common.user.User;


public interface UserDao {

	User findById(int id);
	
	User findBySSO(String sso);
	
	void save(User user);
	
	void deleteBySSO(String sso);
	
	List<User> findAllUsers();

	List<User> findAllPendingAuth();

}

