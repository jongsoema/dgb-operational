package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.ApplicationDetail;

public interface ApplicationDetailDao {
    ApplicationDetail findById(String userId);
}
