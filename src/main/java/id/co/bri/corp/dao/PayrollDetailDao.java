package id.co.bri.corp.dao;

import id.co.bri.corp.model.och.PayrollDetail;

import java.util.List;

public interface PayrollDetailDao {
    PayrollDetail findById(String applicationId);

    List<PayrollDetail> getaAllPayrollAccount();
}
