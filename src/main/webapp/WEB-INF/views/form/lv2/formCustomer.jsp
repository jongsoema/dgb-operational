<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ include file="../../index.jsp" %>

<section id="main-content">
    <input type="hidden" name="pageNo" id="pageNo" value="1" />
    <input type="text" name="userLogin" id="userLogin" value="${pageContext.request.remoteUser}" />
    <input type="text" name="principal" id="principal" value="${principal}" />
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-group pull-right" style="margin-top:10px;margin-left:5px"><!--button id="btnAdd" type="button" class="btn btn-default" data-toggle="tooltip" title="Add New Record"><span class="icon_plus_alt"></span></button --><a class="btn btn-default" data-toggle="modal" href="#myModal2"><span class="icon_search-2" data-toggle="tooltip" title="Advance Search"></span></a></div>
                <table id="table" data-height="545" data-toggle="table" data-show-columns="true" data-show-toggle="true" data-show-pagination-switch="true" data-search="true" data-pagination="true" data-key-events="true"></table>
            </div>
        </div>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addLabel">Advance Search</h4>
                    </div>
                    <div class="modal-body">
                        <form:form class="form-horizontal" role="form" modelAttribute="customers" action="${pageContext.request.contextPath}/advanceSearchCld" method="post" >
                            <div class="form-group">
                                <div class="col-lg-offset-10 col-lg-2">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<script>
    function detailFormatter(index, row) {
        var html = [];
        var ctr = 0;
        $.each(row, function (key, value) {
            if (ctr == 0) {
                html.push('<input type=hidden id=\"'+index+'\" name=\"' + index + '\" value=\"' + key + ',' + value + '\" >');
                ctr = 1;
            }
        });
        html.push('<div class=\"btn-row\" style="margin: auto"><div class=\"btn-group btn-group-sm\"><button data-toggle=\"modal\" data-target=\"#modalTable\" id="btnEdit" type=\"button\" class=\"btn\">Claim</button><button id="btnDelete" type=\"button\" class=\"btn\">Release</button></div></div>');
        return html.join('');
    }
</script>
<script type="text/javascript">
    var $table = $('#table');
    $table.on('click', 'tbody > tr > td', function (e){
        var table = $table.data('bootstrap.table'),
            $element = $(this),
            $tr = $element.parent(),
            row = table.data[$tr.data('index')],
            cellIndex = $element[0].cellIndex,
            $headerCell = table.$header.find('th:eq(' + cellIndex + ')'),
            field = $headerCell.data('field'),
            value = row[field];
        if (cellIndex == 5) {
            table.$el.trigger($.Event('click-cell.bs.table'), [value, row, $element]);
        }
        else if (cellIndex == 0) {
            var claim_val = $tr.find("td:eq(9)").text();
            var recNo = $tr.find("td:eq(2)").text();
            var host = window.location.host;
            var ctx = "${pageContext.request.contextPath}";
            if (claim_val == 0) {
                document.location.href="http://"+host+ctx+'/form/customer/activity/'+recNo+'/action/claim';
            }
            else {
                document.location.href="http://"+host+ctx+'/form/customer/activity/'+recNo+'/action/unclaim';
            }
        }
    });

    $table.on('click-cell.bs.table', function(e, value, row, $element){
        if (value.indexOf("field") < 0) {
            var claimVal = row['field8'], userClaim = row['field9'], isUserTask = 0, userName = "<c:out value='${principal}'/>";
            if (claimVal == 1 && userClaim == userName) {
                isUserTask =  1;
            }
            var host = window.location.host;
            var ctx = "${pageContext.request.contextPath}";
            var url = "http://"+host+ctx+'/form/customer/dtl/'+value+'/'+isUserTask;
            document.location.href = url;
        }
    });

</script>
<script type="text/javascript">
    var $table = $('#table');
    $(function () {
        buildTable($table);
    });
    function buildTable($el) {
        var cells = "<c:out value='${totalColumn}'/>";
        var title = "<c:out value='${title}'/>";
        var titleArr = title.split(',');
        var i, j, row, columns = [], data = [];
        for (i = 0; i < cells; i++) {
            if (i == 1) {
                columns.push({
                    field: 'field' + i,
                    checkbox:true
                });
            }
            else if (i == 0) {
                columns.push({
                    field: 'field' + i,
                    formatter: 'statusFormatter',
                    width:5
                });
            }
            else {
                columns.push({
                    field: 'field' + i,
                    title: titleArr[i],
                    align: 'center'
                });
            }
        }
        <c:forEach items="${customerList}" var="customers">
        i = 0;
        row = {};
        var test1;
        var test2;
        for (j = 0; j < cells; j++) {
            switch (j) {
                case 2:
                    row['field' + j] = ${customers.id};
                    break;
                case 3:
                    test1 = "<c:out value='${customers.emailAddress}'/>";
                    row['field' + j] = test1;
                    break;
                case 4:
                    test1 = "<c:out value='${customers.accountNo}'/>";
                    row['field' + j] = test1;
                    break;
                case 5:
                    test1 = "<c:out value='${customers.cif}'/>";
                    row['field' + j] = test1;
                    break;
                case 6:
                    test1 = "<c:out value='${customers.state.toLowerCase()}'/>";
                    row['field' + j] = test1;
                    break;
                case 7:
                    test1 = "<c:out value='${customers.processId}'/>";
                    row['field' + j] = test1;
                    break;
                case 8:
                    test1 = "<c:out value='${customers.isClaimed}'/>";
                    row['field' + j] = test1;
                    break;
                case 9:
                    test1 = "<c:out value='${customers.taskOwner}'/>";
                    row['field' + j] = test1;
                    break;
                case 10:
                    test1 = "<c:out value='${customers.userGroup}'/>";
                    row['field' + j] = test1;
                    break;
                case 11:
                    test1 = "<c:out value='${customers.onBoardingDate}'/>";
                    row['field' + j] = test1;
                    break;
                case 12:
                    test1 = "<c:out value='${customers.startWfDate}'/>"
                    row['field' + j] = test1;
                    break;
                case 13:
                    test1 = "<c:out value='${a:getKycStatus(customers.kycStatus)}'/>"
                    row['field' + j] = test1;
                    break;
                case 14:
                <c:choose>
                <c:when test="${customers.nasabahExisting eq 0}">
                    row['field' + j] = 'Tidak';
                </c:when>
                <c:otherwise>
                    row['field' + j] = 'Ya';
                </c:otherwise>
                </c:choose>
                    break;
            }
        }
        i++;

        data.push(row);
        </c:forEach>
        $el.bootstrapTable('destroy').bootstrapTable({
            columns: columns,
            search: true,
            data: data
        });
        $el.bootstrapTable('hideLoading');
    }
</script>
<script>
    $( document ).ready(function() {
        jQuery('#main-content').css({
            'margin-left': '0px'
        });
        $("#table tbody > tr").each(function(){
            $(this).find("td:eq(6)").css({
                'text-align':'left','font-size':'smaller'
            });
            $(this).find("td:eq(7)").css({
                'text-align':'left','font-size':'smaller'
            });
        });
    });
</script>
<script>
    $( window ).load(function() {
        $("#table tbody > tr").each(function(){
            var kycStatus = $(this).find("td:eq(13)").text();
            var claim_val = $(this).find("td:eq(8)").text();
            var userTaskOwner = $(this).find("td:eq(9)").text();
            var state = $(this).find("td:eq(6)").text();
            var userLogin = document.getElementById("userLogin").value;

            if ((claim_val == 1) && (userTaskOwner != userLogin)) {
                $(this).css({
                    'pointer-events':'none'
                });
            }
            else if ((claim_val == 1 && (state == "rejected")) || (claim_val == 0 && (state == "rejected"))) {
                $(this).css({
                    'pointer-events':'none'
                });
            }
            else if ((claim_val == 1 && (state == "approved")) || (claim_val == 0 && (state == "approved"))) {
                $(this).css({
                    'pointer-events':'none'
                });
            }
            if (kycStatus !='Kyc-Normal' && kycStatus.length > 0) {
                $(this).css({
                    'background':'#64FFDA'
                });
            }
        });

    });
</script>
<script>
    function statusFormatter(value, row, index) {
        return '<span class="label label-success glyphicon glyphicon-list-alt" style="background-color: #00E676"> </span>'
    }
</script>