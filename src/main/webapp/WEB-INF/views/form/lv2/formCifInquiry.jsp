<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ include file="../../index.jsp" %>

<style>
    .text-on-pannel {
        background: #fff none repeat scroll 0 0;
        height: auto;
        margin-left: 20px;
        padding: 3px 5px;
        position: absolute;
        margin-top: -47px;
        border: 1px solid #337ab7;
        border-radius: 8px;
    }

    .panel {
        /* for text on pannel */
        margin-top: 27px !important;
    }

    .panel-body {
        padding-top: 30px !important;
    }
</style>

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="container">
                <div class="col-lg-10">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h3 class="text-on-pannel text-primary"><strong class="text-uppercase"> Find Customer</strong></h3>
                            <form id="searchByMobile" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/cif/searched" title="">
                                <div>
                                    <label class="title">Mobile No : </label>
                                    <input type="text" id="userId" name="userId" >
                                    <span><input type="submit" id="submitButton"  name="submitButton" value="Submit" onclick="executeAction"></span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-lg-10">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <h3 class="text-on-pannel text-primary"><strong class="text-uppercase"> Customer Profile</strong></h3>
                            <form id='formSearchNasabah' modelAttribute="applicationDetail" action="#" class="formDataPribadi form-horizontal" role="form" method="post" cssStyle="border-radius: 0%">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.name" class="control-label">Nama Lengkap</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.name" type="text" class="form-control" required="true" value="${applicationDetail.name}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.place_of_birth" class="control-label">Tempat Lahir</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.place_of_birth" type="text" class="form-control" required="true" value="${applicationDetail.place_of_birth}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.date_of_birth" class="control-label">Tempat Tanggal Lahir</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.date_of_birth" type="text" class="form-control" required="true" value="${a:formatingDate(applicationDetail.date_of_birth)}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="" class="control-label">Alamat Tempat Tinggal</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="" type="text" class="form-control" required="true" value="${applicationDetail.addr_line_1}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="" class="control-label">RT</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="" type="text" class="form-control" required="true" value="${applicationDetail.addr_line_2}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="" class="control-label">RW</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="" type="text" class="form-control" required="true" value="${applicationDetail.addr_line_3}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="" class="control-label">Kelurahan</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="" type="text" class="form-control" required="true" value="${applicationDetail.kelurahan.cd_desc}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="" class="control-label">Kecamatan</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="" type="text" class="form-control" required="true" value="${applicationDetail.kecamatan.cd_desc}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="" class="control-label">Kota</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="" type="text" class="form-control" required="true" value="${applicationDetail.customerCity.cd_desc}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.postal_code" class="control-label">Provinsi</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.postal_code" type="text" class="form-control" required="true" value="${applicationDetail.province.state_desc}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.postal_code" class="control-label">Kode Pos</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.postal_code" type="text" class="form-control" required="true" value="${applicationDetail.postal_code}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.email_id" class="control-label">Alamat Email</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.email_id" type="text" class="form-control" required="true" value="${applicationDetail.email_id}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.mobile_no" class="control-label">Nomor Handphone</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.mobile_no" type="text" class="form-control" required="true" value="${applicationDetail.mobile_no}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" >
                                            <label path="applicationDetail.appContactDetail.mother_name" class="control-label">Nama Gadis Kandung</label>
                                        </td>
                                        <td colspan="1" >
                                            <input path="applicationDetail.appContactDetail.mother_name" type="text" class="form-control" required="true" value="${applicationDetail.appContactDetail.mother_name}" readonly="true"/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>