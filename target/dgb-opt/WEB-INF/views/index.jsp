<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://localhost:7778/pinang-opt/functions" prefix="a" %>
<!DOCTYPE html>
<html lang="en" oncontextmenu="return false">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pinang Operational Dashboard">
    <meta name="author" content="Taufiq">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <title>PINANG - REPORTING</title>
    <link href="<c:url value='/static/css/taufiq.min.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/taufiq-theme.min.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/elegant-icons-style.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/font-awesome.min.css' />" rel="stylesheet">
    <link href="<c:url value='/static/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css' />" rel="stylesheet">
    <link href="<c:url value='/static/assets/fullcalendar/fullcalendar/fullcalendar.css' />" rel="stylesheet">
    <link href="<c:url value='/static/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css' />" rel="stylesheet" type="text/css" media="screen"/>
    <link href="<c:url value='/static/css/owl.carousel.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/jquery-jvectormap-1.2.2.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/fullcalendar.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/widgets.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/style.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/style-responsive.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/xcharts.min.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/jquery-ui-1.10.4.min.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/lib/taufiq-table.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/lib/bootstrap-editable.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/toggle/bootstrap-toggle.min.css'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/custom/panel.css.old'/>" rel="stylesheet">
    <link href="<c:url value='/static/css/btmin/bootstrap-select.min.css' />" rel="stylesheet">
    <%--<link href="<c:url value='/static/css/tree/bootstrap-treeview.min.css' />" rel="stylesheet">--%>
    <%--<link href="<c:url value='/static/css/tree2/highCheckTree.css' />" rel="stylesheet">--%>

    <link href="<c:url value='/static/css/tree3/style.css' />" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="<c:url value="/static/js/html5shiv.js" />"></script>
    <script src = "<c:url value="/static/js/respond.min.js" />" ></script>
    <script src="<c:url value="/static/js/lte-ie7.js" />"></script>
    <![endif]-->
</head>

<%--<body ondragstart="return false;" ondrop="return false;">--%>
<body ondragstart="return false;" ondrop="return false;">
<section id="container" class="">
    <header class="header dark-bg">
        <%--<div class="toggle-nav">--%>
            <%--<div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>--%>
        <%--</div>--%>
        <a href="#" class="logo">PINANG <span class="lite"> OPERATIONAL</span></a>

        <div class="nav search-row" id="top_menu">
        </div>
        <div class="top-nav notification-row">
            <ul class="nav pull-right top-menu">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="registration">
                        <span class="username">Registration</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="application">
                        <span class="username">Application</span>
                        <%--<b class="caret"></b>--%>
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="disbursement">
                        <span class="username">Disbursement</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="repayment">
                        <span class="username">Repayment</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="paidOff">
                        <span class="username">Paid Off</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="cif">
                        <span class="username">CIF</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="collectibility">
                        <span class="username">Collectibility</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/loanDisbursed' id="collect1">Collect 1</a>
                        </li>
                        <li>
                            <a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/loanDisbursed' id="collect2">Collect 2</a>
                        </li>
                        <li>
                            <a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/loanDisbursed' id="collect3">Collect 3</a>
                        </li>
                        <li>
                            <a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/loanDisbursed' id="collect4">Collect 4</a>
                        </li>
                        <li>
                            <a href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/loanDisbursed' id="collect5">Collect 5</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">Security Maintenance</span>
                        <b class="caret"></b>
                    </a>
                    <%--<ul class="dropdown-menu extended logout">--%>
                    <ul class="dropdown-menu extended">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href='#' id="userManagement">User</a>
                        </li>
                        <li>
                            <a href='#' id="userManagement">Role</a>
                        </li>
                    </ul>
                </li>
                <li id="task_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-envelope-l"></i><span class="badge bg-important">0</span>
                    </a>
                    <ul class="dropdown-menu extended tasks-bar">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li class="external">
                            <a href="#">See All Notifications</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">${fn:toUpperCase(pageContext.request.remoteUser)}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="#"><i class="icon_key_alt"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="<c:url value="/logout" />"><i class="icon_briefcase_alt"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>
</section>
<script src="<c:url value="/static/js/jquery.js" />"></script>
<script src="<c:url value="/static/js/jquery-ui-1.10.4.min.js" />"></script>
<script src="<c:url value="/static/js/jquery-1.8.3.min.js" />"></script>
<script src="<c:url value="/static/js/jquery-ui-1.9.2.custom.min.js" />"></script>
<script src="<c:url value="/static/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/static/js/jquery.scrollTo.min.js" />"></script>
<script src="<c:url value="/static/js/jquery.nicescroll.js" />"></script>
<script src="<c:url value="/static/assets/jquery-knob/js/jquery.knob.js" />"></script>
<script src="<c:url value="/static/js/jquery.sparkline.js" />"></script>
<script src="<c:url value="/static/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" />"></script>
<script src="<c:url value="/static/js/scripts.js" />"></script>
<script src="<c:url value="/static/js/jquery-jvectormap-1.2.2.min.js" />"></script>
<script src="<c:url value="/static/js/jquery-jvectormap-world-mill-en.js" />"></script>
<script src="<c:url value="/static/js/xcharts.min.js" />"></script>
<script src="<c:url value="/static/js/jquery.autosize.min.js" />"></script>
<script src="<c:url value="/static/js/jquery.placeholder.min.js" />"></script>
<script src="<c:url value="/static/js/gdp-data.js" />"></script>
<script src="<c:url value="/static/js/morris.min.js" />"></script>
<script src="<c:url value="/static/js/sparklines.js" />"></script>
<script src="<c:url value="/static/js/charts.js.old" />"></script>
<script src="<c:url value="/static/js/jquery.slimscroll.min.js" />"></script>
<script src="<c:url value="/static/js/lib/taufiq-table.js" />"></script>
<script src="<c:url value="/static/js/lib/key_event.js" />"></script>
<script src="<c:url value="/static/js/lib/taufiq-acct.min.js" />"></script>
<script src="<c:url value="/static/js/lib/bootstrap-table-editable.js" />"></script>
<script src="<c:url value="/static/js/lib/bootstrap-editable.js" />"></script>
<script src="<c:url value="/static/js/lib/ext/bootstrap-select.min.js" />"></script>
<script src="<c:url value="/static/js/lib/tblExp.js" />"></script>
<script src="<c:url value="/static/js/lib/booty-table-exp.min.js" />"></script>
<script src="<c:url value="/static/js/ga.js" />"></script>

<script language="javascript" type="text/javascript">
    var host = window.location.host;
    var ctx = "${pageContext.request.contextPath}";
    $("#registration" ).click(function() {
        document.location.href = "http://" + host + ctx + '/form/group/registration';
    });
    $("#application" ).click(function() {
        document.location.href = "http://" + host + ctx + '/form/group/application';
    });
    $("#disbursement" ).click(function() {
        document.location.href = "http://" + host + ctx + '/form/group/loanDisbursed';
    });
    $("#repayment" ).click(function() {
        document.location.href = "http://" + host + ctx + '/form/group/repayment';
    });
    $("#paidOff" ).click(function() {
        document.location.href = "http://" + host + ctx + '/form/group/paidOff';
    });
    $("#cif" ).click(function() {
        document.location.href = "http://" + host + ctx + '/form/group/cif';
    });
</script>

</body>
</html>