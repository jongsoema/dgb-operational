<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="BRIAGRO">
    <meta name="keyword" content="Jenius-Eform">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>Login Page</title>
    <link href="<c:url value='/static/css/taufiq.min.css' />"  rel="stylesheet"/>
    <link href="<c:url value='/static/css/taufiq-theme.min.css' />"  rel="stylesheet"/>
    <link href="<c:url value='/static/css/elegant-icons-style.css' />"  rel="stylesheet"/>
    <link href="<c:url value='/static/css/font-awesome.css' />"  rel="stylesheet"/>
    <link href="<c:url value='/static/css/style.css' />"  rel="stylesheet"/>
    <link href="<c:url value='/static/css/style-responsive.css' />"  rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="<c:url value="/static/js/html5shiv.js" />"></script>
    <script src="<c:url value="/static/js/respond.min.js" />"></script>
    <![endif]-->
</head>
<div class="navbar-default navbar-fixed-top">
    <div class="bd-pageheader text-center" style="background-color: #3E2723;padding-top: 10px;padding-bottom: 10px">
        <img src="<c:url value='/static/img/logo/logo.png' />" class="img-rounded" style="width: 150px;height: 80px">
    </div>
</div>
<body class="login-body-2" oncontextmenu="return false">
<div class="container">
    <c:url var="loginUrl" value="/login" />
    <form class="login-form" action="${loginUrl}" method="post" style="margin-top:25%">
        <div class="login-wrap">
            <c:if test="${param.error != null}">
                <div class="alert alert-danger">
                    <p>Invalid username and password.</p>
                </div>
            </c:if>
            <p class="login-img">
                <%--<img src="<c:url value='/static/img/logo/logo.png' />" class="img-rounded" style="width: 150px;height: 100px">--%>
            </p>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input id="username" name="ssoId" type="text" class="form-control" placeholder="Username" autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input id="password" name="password" type="password" class="form-control" placeholder="Password">
            </div>
            <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
            <button class="btn btn-warning btn-lg btn-block" type="submit">Login</button>
        </div>
    </form>
</div>
<script language="javascript">
    document.onmousedown=disableclick;
    function disableclick(event) {
        if(event.button==2) {
            return false;
        }
    }
</script>
<div class="navbar-default navbar-fixed-bottom">
    <div class="bd-pageheader text-center" style="background-color: #3E2723;">
        <div class="container" style="height: 80px">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <p class="card-title" style="margin:25px;text-align: center;color: #9E9E9E;font-family:'Times New Roman'">@2019 BRIAGRO, All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>