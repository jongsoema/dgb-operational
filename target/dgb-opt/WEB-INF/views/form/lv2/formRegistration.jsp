<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ include file="../../index.jsp" %>

<section id="main-content">
    <input type="hidden" name="pageNo" id="pageNo" value="1" />
    <input type="text" name="userLogin" id="userLogin" value="${pageContext.request.remoteUser}" />
    <input type="text" name="principal" id="principal" value="${principal}" />
    <section class="wrapper">
        <div class="row">
            <div class="container">
                <div class="col-lg-12">
                    <div class="btn-group pull-right" style="margin-top:10px;margin-left:5px"><!--button id="btnAdd" type="button" class="btn btn-default" data-toggle="tooltip" title="Add New Record"><span class="icon_plus_alt"></span></button --><a class="btn btn-default" data-toggle="modal" href="#myModal2"><span class="icon_search-2" data-toggle="tooltip" title="Advance Search"></span></a></div>
                    <%--<table id="table" data-height="545" data-detail-view="true" data-detail-formatter="detailFormatter" data-toggle="table" data-show-columns="true" data-show-toggle="true" data-show-pagination-switch="true" data-search="true" data-pagination="true" data-key-events="true"></table>--%>
                    <table id="table" data-height="545" data-toggle="table" data-show-pagination-switch="true" data-search="true" data-pagination="true"></table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addLabel">Advance Search</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<script>
    function detailFormatter(index, row) {
        var html = [];
        var ctr = 0;
        $.each(row, function (key, value) {
            if (ctr == 0) {
                html.push('<input type=hidden id=\"'+index+'\" name=\"' + index + '\" value=\"' + key + ',' + value + '\" >');
                ctr = 1;
            }
        });
        html.push('<div class=\"btn-row\" style="margin: auto"><div class=\"btn-group btn-group-sm\"><button data-toggle=\"modal\" data-target=\"#modalTable\" id="btnEdit" type=\"button\" class=\"btn\">Claim</button><button id="btnDelete" type=\"button\" class=\"btn\">Release</button></div></div>');
        return html.join('');
    }
</script>
<script type="text/javascript">
    var $table = $('#table');
    $(function () {
        buildTable($table);
    });
    function buildTable($el) {
        var cells = "<c:out value='${totalColumn}'/>";
        var title = "<c:out value='${title}'/>";
        var titleArr = title.split(',');
        var i, j, row, columns = [], data = [];
        for (i = 0; i < cells; i++) {
            if (i == 0) {
                columns.push({
                    field: 'field' + i,
                    formatter: 'statusFormatter',
                    width:5
                });
            }
            else if (i == 2 || i == 3) {
                columns.push({
                    field: 'field' + i,
                    title: titleArr[i],
                    align: 'center',
                    sortable:'true'
                });
            }
            else {
                columns.push({
                    field: 'field' + i,
                    title: titleArr[i],
                    align: 'center'
                });
            }
        }
        <c:forEach items="${registrationList}" var="registration">
        i = 0;
        row = {};
        var test1;
        for (j = 0; j < cells; j++) {
            switch (j) {
                case 1:
                    test1 = "<c:out value='${registration.userId}'/>";
                    row['field' + j] = test1;
                    break;
                case 2:
                    test1 = "<c:out value='${registration.r_cre_time}'/>";
                    row['field' + j] = test1;
                    break;
                case 3:
                    test1 = "<c:out value='${registration.r_mod_time}'/>";
                    row['field' + j] = test1;
                    break;
            }
        }
        data.push(row);
        </c:forEach>
        $el.bootstrapTable('destroy').bootstrapTable({
            columns: columns,
            search: true,
            data: data
        });
        $el.bootstrapTable('hideLoading');
    }
</script>
<script>
    $( document ).ready(function() {
        jQuery('#main-content').css({
            'margin-left': '0px'
        });
    });
    $( window ).load(function() {

    });
</script>
<script>
    function statusFormatter(value, row, index) {
        return '<span class="label label-success glyphicon glyphicon-list-alt" style="background-color: #00E676"> </span>'
    }
</script>