<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ include file="../../index.jsp" %>

<section id="main-content">
    <input type="hidden" name="pageNo" id="pageNo" value="1" />
    <input type="text" name="userLogin" id="userLogin" value="${pageContext.request.remoteUser}" />
    <input type="text" name="principal" id="principal" value="${principal}" />
    <section class="wrapper">
        <div class="row">
            <div class="container">
                <div class="col-lg-12">
                    <div class="btn-group pull-right" style="margin-top:10px;margin-left:5px"><!--button id="btnAdd" type="button" class="btn btn-default" data-toggle="tooltip" title="Add New Record"><span class="icon_plus_alt"></span></button --><a class="btn btn-default" data-toggle="modal" href="#myModal2"><span class="icon_search-2" data-toggle="tooltip" title="Advance Search"></span></a></div>
                    <table id="table" data-height="545" data-toggle="table" data-show-pagination-switch="true" data-search="true" data-pagination="true"></table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <mvc:form id='formBaseFilterDisburse' modelAttribute="baseFilter" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/form/group/loanDisbursedFiltered" class="formBaseFilterDisburse form-group form-horizontal" role="form" method="post" cssStyle="border-radius: 0%;display: block;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="font-weight: bold;text-align: center;">Advance Search Data Disbursement</h4>
                        </div>
                        <div class="modal-body" style="height: 300px !important;width:auto!important;overflow-y: auto;!important;">
                            <div class="row col-lg-12 text-center">
                                <mvc:label path="" class="control-label text-center"><span style="font-style: italic">Filtering Disbursement </span><span style="font-weight: bold"></span></mvc:label>
                            </div>
                            <div class="row col-lg-12" style="margin-bottom: 2%">
                                <div class="row col-lg-4 col-lg-offset-1">
                                    <div class="text-left"><mvc:label path="fromDate" class="control-label text-center" cssStyle="font-weight: bold;font-family: 'Chalkboard SE'">From Date</mvc:label></div>
                                    <div class="text-left">
                                        <mvc:input path="fromDate" type="text" class="form-control" required="true" value="" id="filterFromDate"/>
                                    </div>
                                </div>
                                <div class="row col-lg-4 col-lg-offset-2">
                                    <div class="text-left"><mvc:label path="userId" class="control-label text-center" cssStyle="font-weight: bold;font-family: 'Chalkboard SE'">User Id</mvc:label></div>
                                    <div class="text-left">
                                        <mvc:input path="userId" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-lg-12" style="margin-bottom: 2%">
                                <div class="row col-lg-4 col-lg-offset-1">
                                    <div class="text-left"><mvc:label path="" class="control-label text-center" cssStyle="font-weight: bold;font-family: 'Chalkboard SE'">To Date</mvc:label></div>
                                    <div class="text-left">
                                        <mvc:input path="toDate" type="text" class="form-control" required="true" value=""  id="filterToDate"/>
                                    </div>
                                </div>
                                <div class="row col-lg-4 col-lg-offset-2">
                                    <div class="text-left"><mvc:label path="accountName" class="control-label text-center" cssStyle="font-weight: bold;font-family: 'Chalkboard SE'">User Name</mvc:label></div>
                                    <div class="text-left">
                                        <mvc:input path="accountName" type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-lg-12" style="margin-bottom: 2%">
                                <div class="row col-lg-4 col-lg-offset-1">
                                    <div class="text-left"><mvc:label path="status" class="control-label text-center" cssStyle="font-weight: bold">Disbursement Status</mvc:label></div>
                                    <div class="text-left">
                                        <mvc:select path="status" class="form-control" items="${a:getDisburseStatus()}"/>
                                    </div>
                                </div>
                                <div class="row col-lg-4 col-lg-offset-2">
                                    <div class="text-left"><mvc:label path="loanAccount" class="control-label text-center" cssStyle="font-weight: bold">Loan Account</mvc:label></div>
                                    <div class="text-left">
                                        <mvc:input path="loanAccount" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group">
                                <div class="col-lg-offset-10 col-lg-2">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="submit" class="btn btn-md btn-warning">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </mvc:form>
        </div>
    </section>
</section>

<script>
    function detailFormatter(index, row) {
        var html = [];
        var ctr = 0;
        $.each(row, function (key, value) {
            if (ctr == 0) {
                html.push('<input type=hidden id=\"'+index+'\" name=\"' + index + '\" value=\"' + key + ',' + value + '\" >');
                ctr = 1;
            }
        });
        html.push('<div class=\"btn-row\" style="margin: auto"><div class=\"btn-group btn-group-sm\"><button data-toggle=\"modal\" data-target=\"#modalTable\" id="btnEdit" type=\"button\" class=\"btn\">Claim</button><button id="btnDelete" type=\"button\" class=\"btn\">Release</button></div></div>');
        return html.join('');
    }
</script>
<script type="text/javascript">
    var $table = $('#table');
    $(function () {
        buildTable($table);
    });
    function buildTable($el) {
        var cells = "<c:out value='${totalColumn}'/>";
        var title = "<c:out value='${title}'/>";
        var titleArr = title.split(',');
        var i, j, row, columns = [], data = [];
        for (i = 0; i < cells; i++) {
            if (i == 0) {
                columns.push({
                    field: 'field' + i,
                    formatter: 'statusFormatter',
                    width:5
                });
            }
            else if (i == 8) {
                columns.push({
                    field: 'field' + i,
                    title: titleArr[i],
                    align: 'center',
                    sortable:'true'
                });
            }
            else {
                columns.push({
                    field: 'field' + i,
                    title: titleArr[i],
                    align: 'center'
                });
            }
        }
        <c:forEach items="${fundTransferList}" var="fundTransfer">
        i = 0;
        row = {};
        var test1;
        for (j = 0; j < cells; j++) {
            switch (j) {
                case 1:
                    test1 = "<c:out value='${fundTransfer.disbursement.user_id}'/>";
                    row['field' + j] = test1;
                    break;
                case 2:
                    test1 = "<c:out value='${fundTransfer.disbursement.applicationDetail.national_id}'/>";
                    row['field' + j] = test1;
                    break;
                case 3:
                    <c:choose>
                        <c:when test="${not empty fundTransfer.disbursement.payrollDetail.name_wl}">
                            test1 = "<c:out value='${fundTransfer.disbursement.payrollDetail.name_wl}'/>";
                            row['field' + j] = test1;
                        </c:when>
                        <c:otherwise>
                            row['field' + j] = 'No Name';
                        </c:otherwise>
                    </c:choose>
                    break;
                case 4:
                    test1 = "<c:out value='${fundTransfer.disbursement.loan_account_id}'/>";
                    row['field' + j] = test1;
                    break;
                case 5:
                    test1 = "<c:out value='${fundTransfer.disbursement.approved_amount}'/>";
                    row['field' + j] = test1;
                    break;
                case 6:
                    test1 = "<c:out value='${fundTransfer.disbursement.requested_tenor}'/>";
                    row['field' + j] = test1;
                    break;
                case 7:
                    test1 = "<c:out value='${fundTransfer.source_of_fund}'/>";
                    row['field' + j] = test1;
                    break;
                case 8:
                    test1 = "<c:out value='${fundTransfer.destination_of_fund}'/>";
                    row['field' + j] = test1;
                    break;
                case 9:
                    test1 = "<c:out value='${fundTransfer.transaction_date_time}'/>";
                    row['field' + j] = test1;
                    break;
                case 10:
                    test1 = "<c:out value='${fundTransfer.transaction_remarks}'/>";
                    row['field' + j] = test1;
                    break;
                case 11:
                <c:choose>
                <c:when test="${fundTransfer.status eq 'S'}">
                    row['field' + j] = 'Success';
                </c:when>
                <c:otherwise>
                    row['field' + j] = 'Failed';
                </c:otherwise>
                </c:choose>
                    break;
                case 12:
                    test1 = "<c:out value='${fundTransfer.disbursement.payrollDetail.company_name}'/>";
                    row['field' + j] = test1;
                    break;
            }
        }
        data.push(row);
        </c:forEach>
        $el.bootstrapTable('destroy').bootstrapTable({
            columns: columns,
            search: true,
            data: data
        });
        $el.bootstrapTable('hideLoading');
    }
</script>
<script>
    $( document ).ready(function() {
        jQuery('#main-content').css({
            'margin-left': '0px'
        });
        $("#table tbody > tr").each(function(){
        });
        $(".disableEvent").on("contextmenu", function (e) {
            return false;
        })
    });
    $( window ).load(function() {

    });
</script>
<script>
    function statusFormatter(value, row, index) {
        return '<span class="label label-success glyphicon glyphicon-list-alt" style="background-color: #00E676"> </span>'
    }
</script>
<script>
    $( function() {
        $( "#filterFromDate" ).datepicker();
        $( "#filterToDate" ).datepicker();
    } );
</script>