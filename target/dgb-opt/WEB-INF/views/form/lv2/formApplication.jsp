<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ include file="../../index.jsp" %>

<section id="main-content">
    <input type="hidden" name="pageNo" id="pageNo" value="1" />
    <input type="text" name="userLogin" id="userLogin" value="${pageContext.request.remoteUser}" />
    <input type="text" name="principal" id="principal" value="${principal}" />
    <section class="wrapper">
        <div class="row">
            <div class="container">
                <div class="col-lg-12">
                    <div class="btn-group pull-right" style="margin-top:10px;margin-left:5px"><!--button id="btnAdd" type="button" class="btn btn-default" data-toggle="tooltip" title="Add New Record"><span class="icon_plus_alt"></span></button --><a class="btn btn-default" data-toggle="modal" href="#myModal2"><span class="icon_search-2" data-toggle="tooltip" title="Advance Search"></span></a></div>
                    <table id="table" data-height="545" data-toggle="table" data-show-pagination-switch="true" data-search="true" data-pagination="true"></table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addLabel">Advance Search</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<script>
    function detailFormatter(index, row) {
        var html = [];
        var ctr = 0;
        $.each(row, function (key, value) {
            if (ctr == 0) {
                html.push('<input type=hidden id=\"'+index+'\" name=\"' + index + '\" value=\"' + key + ',' + value + '\" >');
                ctr = 1;
            }
        });
        html.push('<div class=\"btn-row\" style="margin: auto"><div class=\"btn-group btn-group-sm\"><button data-toggle=\"modal\" data-target=\"#modalTable\" id="btnEdit" type=\"button\" class=\"btn\">Claim</button><button id="btnDelete" type=\"button\" class=\"btn\">Release</button></div></div>');
        return html.join('');
    }
</script>
<%--<script type="text/javascript">--%>
    <%--var $table = $('#table');--%>
    <%--$table.on('click', 'tbody > tr > td', function (e){--%>
        <%--var table = $table.data('bootstrap.table'),--%>
            <%--$element = $(this),--%>
            <%--$tr = $element.parent(),--%>
            <%--row = table.data[$tr.data('index')],--%>
            <%--cellIndex = $element[0].cellIndex,--%>
            <%--$headerCell = table.$header.find('th:eq(' + cellIndex + ')'),--%>
            <%--field = $headerCell.data('field'),--%>
            <%--value = row[field];--%>
        <%--if (cellIndex == 5) {--%>
            <%--table.$el.trigger($.Event('click-cell.bs.table'), [value, row, $element]);--%>
        <%--}--%>
        <%--else if (cellIndex == 0) {--%>
            <%--var claim_val = $tr.find("td:eq(9)").text();--%>
            <%--var recNo = $tr.find("td:eq(2)").text();--%>
            <%--var host = window.location.host;--%>
            <%--var ctx = "${pageContext.request.contextPath}";--%>
            <%--if (claim_val == 0) {--%>
                <%--document.location.href="http://"+host+ctx+'/form/customer/activity/'+recNo+'/action/claim';--%>
            <%--}--%>
            <%--else {--%>
                <%--document.location.href="http://"+host+ctx+'/form/customer/activity/'+recNo+'/action/unclaim';--%>
            <%--}--%>
        <%--}--%>
    <%--});--%>

    <%--$table.on('click-cell.bs.table', function(e, value, row, $element){--%>
        <%--if (value.indexOf("field") < 0) {--%>
            <%--var claimVal = row['field8'], userClaim = row['field9'], isUserTask = 0, userName = "<c:out value='${principal}'/>";--%>
            <%--if (claimVal == 1 && userClaim == userName) {--%>
                <%--isUserTask =  1;--%>
            <%--}--%>
            <%--var host = window.location.host;--%>
            <%--var ctx = "${pageContext.request.contextPath}";--%>
            <%--var url = "http://"+host+ctx+'/form/customer/dtl/'+value+'/'+isUserTask;--%>
            <%--document.location.href = url;--%>
        <%--}--%>
    <%--});--%>

<%--</script>--%>
<script type="text/javascript">
    var $table = $('#table');
    $(function () {
        buildTable($table);
    });
    function buildTable($el) {
        var cells = "<c:out value='${totalColumn}'/>";
        var title = "<c:out value='${title}'/>";
        var titleArr = title.split(',');
        var i, j, row, columns = [], data = [];
        for (i = 0; i < cells; i++) {
            // if (i == 1) {
            //     columns.push({
            //         field: 'field' + i,
            //         checkbox:true
            //     });
            // }
            if (i == 0) {
                columns.push({
                    field: 'field' + i,
                    formatter: 'statusFormatter',
                    width:5
                });
            }
            else {
                columns.push({
                    field: 'field' + i,
                    title: titleArr[i],
                    align: 'center'
                });
            }
        }
        <c:forEach items="${disbursementList}" var="disbursement">
        i = 0;
        row = {};
        var test1;
        for (j = 0; j < cells; j++) {
            switch (j) {
                case 1:
                    test1 = "<c:out value='${disbursement.user_id}'/>";
                    row['field' + j] = test1;
                    break;
                case 2:
                    test1 = "<c:out value='${disbursement.applicationDetail.national_id}'/>";
                    row['field' + j] = test1;
                    break;
                case 3:
                    test1 = "<c:out value='${disbursement.payrollDetail.name_wl}'/>";
                    row['field' + j] = test1;
                    break;
                case 4:
                    test1 = "<c:out value='${disbursement.application_id}'/>";
                    row['field' + j] = test1;
                    break;
                case 5:
                    test1 = "<c:out value='${a:numberToText(disbursement.requested_loan_amt)}'/>";
                    row['field' + j] = test1;
                    break;
                case 6:
                    test1 = "<c:out value='${a:numberToText(disbursement.approved_amount)}'/>";
                    row['field' + j] = test1;
                    break;
                case 7:
                    test1 = "<c:out value='${disbursement.requested_tenor}'/>";
                    row['field' + j] = test1;
                    break;
                case 8:
                    test1 = "<c:out value='${disbursement.application_status}'/>";
                    row['field' + j] = test1;
                    break;
                case 9:
                    test1 = "<c:out value='${disbursement.r_cre_time}'/>";
                    row['field' + j] = test1;
                    break;
                case 10:
                    test1 = "<c:out value='${disbursement.r_mod_time}'/>";
                    row['field' + j] = test1;
                    break;
                case 11:
                    test1 = "<c:out value='${disbursement.payrollDetail.company_name}'/>";
                    row['field' + j] = test1;
                    break;
            }
        }
        data.push(row);
        </c:forEach>
        $el.bootstrapTable('destroy').bootstrapTable({
            columns: columns,
            search: true,
            data: data
        });
        $el.bootstrapTable('hideLoading');
    }
</script>
<script>
    $(document).ready(function() {
        jQuery('#main-content').css({
            'margin-left': '0px'
        });
        $("#table tbody > tr").each(function(){
            $(this).find("td:eq(5)").css({
                'text-align':'left','font-size':'smaller'
            });
            $(this).find("td:eq(11)").css({
                'text-align':'left','font-size':'smaller'
            });
            $(this).find("td:eq(13)").css({
                'text-align':'left','font-size':'smaller'
            });
        });
    });
    $( window ).load(function() {
        $("#table tbody > tr").each(function(){
            $(this).find("td:eq(5)").css({
                'text-align':'left','font-size':'smaller'
            });
            $(this).find("td:eq(11)").css({
                'text-align':'left','font-size':'smaller'
            });
            $(this).find("td:eq(13)").css({
                'text-align':'left','font-size':'smaller'
            });
        });
    });
</script>
<script>
    function statusFormatter(value, row, index) {
        return '<span class="label label-success glyphicon glyphicon-list-alt" style="background-color: #00E676"> </span>'
    }
</script>